#!/usr/bin/env python
# encoding: utf-8

"""
Primeiros Testes com Captcha no TRT2
lab804 - http://www.lab804.com.br - 2014
"""

__author__ = 'murilobsd'

from PIL import Image
from scipy.ndimage import interpolation, filters


def getImage(impath):
    im = Image.open(impath)
    im = im.convert('L')
    return im


def getImageAsNumpy(impath):
    im = getImage(impath)
    im = array(im)
    return im

def erode(npimg, elem_rad=1):
    elem = disk(elem_rad)
    d = dilation (npimg, elem)
    return d

def preprocess(image):
    npimg = getImageAsNumpy(image)
    npimg = filters.gaussian_filter(npimg, 1)
    #npimg = greyThreshold(npimg, 200)
    #npimg = filters.median_filter(npimg,3)
    #npimg = invertBackground(npimg)
    npimg = erode(npimg, 2)
    npimg = greyThreshold(npimg, 165)
    return npimg
