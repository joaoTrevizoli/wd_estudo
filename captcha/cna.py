# encoding: utf-8

__author__ = 'murilobsd'

import requesocks
import time
import os
import datetime
import slugify
import json
from base64 import decodestring
import hashlib
import random
import cPickle

pasta = os.path.dirname(os.path.realpath(__file__))

CHAVE = '{96C63CE2-21E3-4885-B701-86EBDADAA8B0}'

headers = {
    'Accept': 'application/json',
    'token': '',
    'utc-date': '',
    'User-Agent': 'Dalvik/1.6.0 (Linux; U; Android 4.4.2; sdk Build/KK)',
    'Host': 'cna.oab.org.br',
    'Connection': 'Keep-Alive',
    'Accept-Encoding': 'gzip'
}


def base_to_img(arquivo, text):
    with open(os.path.join(pasta, "fotos/" + arquivo + '.jpg'), "wb") as f:
        f.write(decodestring(text))
    return "fotos/arquivo" + '.jpg'


def generate_headers():
    data = datetime.datetime.strftime(datetime.datetime.utcnow(), '%d-%m-%Y %H:%M:%S')
    token = hashlib.md5(data + CHAVE).hexdigest().upper()
    print "Token Gerado com sucesso: %s " % token
    headers['token'] = token
    headers['utc-date'] = data
    return headers


def save(inscricao, advogado, dados):
    with open(os.path.join(pasta, "dados", inscricao + "_" + advogado), "wb") as output_file:
        cPickle.dump(dados, output_file)


def abrir(arquivo):
    with open(arquivo, "rb") as input_file:
        e = cPickle.load(input_file)
    return e


def coletar_info(obj_sessao, url):
    novo_header = generate_headers()
    try:
        conteudo = obj_sessao.get(url, headers=novo_header, verify=False)
    except Exception as e:
        print "Erro na captura das info do advogado: %s." % e
        return
    if conteudo.status_code == 200:
        data_json = json.loads(conteudo.content)
        print "Nome: ", data_json['Nome']
        print "Inscricao: ", data_json['Inscricao']
        hash = hashlib.md5(slugify.slugify(data_json['Nome'].lower()) + data_json['Uf']).hexdigest()
        # img = None
        #if data_json['Foto']:
        #img = base_to_img(hash, data_json['Foto'])
        save(data_json['Inscricao'], hash, data_json)
    else:
        print "Erro: %s" % conteudo.status_code


def coletar_adv(inscricao, estado='SP', tipo='1'):
    if inscricao and estado:
        novo_header = generate_headers()
        host = 'http://cna.oab.org.br/rest/api/service/' \
               'searchPerson?nome=&insc={insc}&uf={estado}&tipoInsc={tipo}'.format(insc=inscricao,
                                                                                   estado=estado.upper(),
                                                                                   tipo=tipo)
        sessao = requesocks.Session()
        sessao.proxies = {'http': 'socks5://127.0.0.1:9050'}
        conteudo = sessao.get(host, headers=novo_header, verify=False)
        if conteudo.status_code == 200:
            if conteudo.content != '[]':
                data_json = json.loads(conteudo.content)
                coletar_info(sessao, data_json[0]['DetailUrl'])
            else:
                print "*** Sem Conteudo para o numero: %s ***" % inscricao
                if os.path.exists(os.path.join(pasta, 'negados')):
                    with open(os.path.join(pasta, 'negados'), "rb") as input_file:
                        e = cPickle.load(input_file)

                    e.append(inscricao)

                    with open(os.path.join(pasta, 'negados'), "wb") as output_file:
                                cPickle.dump(e, output_file)
        else:
            print conteudo.status_code
            print conteudo.content
    else:
        return False


d = []
if not os.path.exists(os.path.join(pasta, 'negados')):
    with open(os.path.join(pasta, 'negados'), "wb") as output_file:
            cPickle.dump(d, output_file)
else:
    with open(os.path.join(pasta, 'negados'), "rb") as input_file:
        d = cPickle.load(input_file)
        print d

numeros_oabs = range(1, 1000000)

random.shuffle(numeros_oabs)

listados = [arq.split('_')[0] for arq in os.listdir(os.path.join(pasta, 'dados'))]

if listados:
    print "Numero ja encontrado(s): %d" % len(listados)

print "Removendo numeros ja encontrados."

for num in listados:
    try:
        n = int(num)
        numeros_oabs.remove(n)
    except:
        pass

print "Nova quantidade de oabs a ser pesquisada: %s" % len(numeros_oabs)

while numeros_oabs:
    try:
        inscr = random.choice(numeros_oabs)
        coletar_adv(inscricao=str(inscr), estado='SP', tipo='1')
        numeros_oabs.remove(inscr)
    except:
        pass
    print "Faltam: %d a ser pesquisados." % len(numeros_oabs)
    time.sleep(0.8)

# 39015, BA, Thatiana poncino do nascimento
# coletar_adv(inscricao='312680', estado='SP', tipo='1')
# tipo 1 adv, 2 est, 3 supl
#count = 34014
#x = 1
#while count > x:
#    coletar_adv(inscricao=str(count), estado='BA', tipo='1')
#    count -= 1
