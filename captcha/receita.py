# encoding: utf-8


# from numpy import array, count_nonzero
#from PIL import Image
#from scipy.ndimage import filters
#from scipy.misc import toimage
#from skimage.morphology import disk, dilation

import requesocks

__author__ = 'murilobsd'

requesicao = u"""<v:Envelope xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:d="http://www.w3.org/2001/XMLSchema" xmlns:c="http://schemas.xmlsoap.org/soap/encoding/" xmlns:v="http://schemas.xmlsoap.org/soap/envelope/">
	<v:Header>
		<n0:token xmlns:n0="http://soap.ws.cpf.service.mobile.rfb.serpro.gov.br/">45b17b94311db0a9b50a86b2f8a3b0b94d2a595d</n0:token>
		<n1:aplicativo xmlns:n1="http://soap.ws.cpf.service.mobile.rfb.serpro.gov.br/">Pessoa F&#237;sica</n1:aplicativo>
		<n2:plataforma xmlns:n2="http://soap.ws.cpf.service.mobile.rfb.serpro.gov.br/">Android</n2:plataforma>
		<n3:versao xmlns:n3="http://soap.ws.cpf.service.mobile.rfb.serpro.gov.br/">4.4.2</n3:versao>
		<n4:dispositivo xmlns:n4="http://soap.ws.cpf.service.mobile.rfb.serpro.gov.br/">Android SDK built for x86</n4:dispositivo>
		<n5:versao_app xmlns:n5="http://soap.ws.cpf.service.mobile.rfb.serpro.gov.br/">3.1</n5:versao_app>
	</v:Header>
	<v:Body>
		<n6:obtemSituacaoCadastral id="o0" c:root="1" xmlns:n6="http://soap.ws.cpf.service.mobile.rfb.serpro.gov.br/">
			<cpf i:type="d:string">77992296915</cpf>
		</n6:obtemSituacaoCadastral>
	</v:Body>
</v:Envelope>
"""

encoded_request = requesicao.encode('utf-8')
encoded_request.format(cpf=u"3405721671")

headers = {
    'Connection': 'keep-alive',
    'User-Agent': 'ksoap2-android/2.6.0+',
    'SOAPAction': 'http://soap.ws.cpf.service.mobile.rfb.serpro.gov.br/obtemSituacaoCadastral',
    'Content-Type': 'text/xml;charset=utf-8',
    'Host': 'movel01.receita.fazenda.gov.br',
    'Accept-Encoding': 'gzip',
    'Content-Length': len(encoded_request)
}

print encoded_request
print headers

"""
def invertBackground(npimg):
    size = npimg.shape
    imarea = size[0]*size[1]
    whitearea = count_nonzero(npimg)
    blackarea = imarea - whitearea
    if whitearea < blackarea:
        npimg = 255-npimg
    return npimg

def getImage(impath):
    im = Image.open(impath)
    im = im.convert('L')
    return im


def getImageAsNumpy(impath):
    im = getImage(impath)
    im = array(im)
    return im


def greyThreshold(npimg, threshold):
    size = npimg.shape
    for row in range(size[0]):
        for col in range(size[1]):
            if npimg[row, col] <= threshold:
                npimg[row, col] = 0
            else:
                npimg[row, col] = 255
    return npimg


def erode(npimg, elem_rad=1):
    elem = disk(elem_rad)
    d = dilation(npimg, elem)
    return d

def preprocess(image):
    npimg = getImageAsNumpy(image)
    npimg = filters.median_filter(npimg, 4)
    npimg = greyThreshold(npimg, 150)
    npimg = invertBackground(npimg)
    npimg = erode(npimg, 2)
    #npimg = greyThreshold(npimg, 150)
    return npimg


def segment(npimg, parts=4):
    [a, b] = [0, 0]
    size = npimg.shape

    for c in range(size[1]):
        if 0 in npimg[:, c]:
            a = c
            break

    for c in reversed(range(size[1])):
        if 0 in npimg[:, c]:
            b = c
            break

    print "End points found:", a, b

    avg_size = (b - a) / parts
    prev = a

    charCuts = [a]

    for i in range(parts):
        if prev + avg_size <= b:
            charCuts.append(prev + avg_size)
            prev = prev + avg_size
        else:
            break

    images = []
    for i in range(len(charCuts) - 1):
        images.append(npimg[:, charCuts[i]:charCuts[i + 1]])
    print "Images Segmented:: ", len(images)
    return images

#b = segment(preprocess('/home/murilobsd/projetos/wd/captcha/images/cna_oab.jpeg'))
#for x in b:
#    toimage(x).show()
toimage(preprocess('/home/murilobsd/projetos/wd/captcha/images/cna_oab.jpeg')).show()
"""

