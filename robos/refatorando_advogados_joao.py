#!/usr/bin/env python
# encoding: utf-8
__author__ = 'joaotrevizoliesteves'

import sys
import unicodedata
import pymongo.errors
from models import *
from utilitarios import fix_bad_unicode
from datetime import datetime
from time import sleep
from hashlib import md5
from gevent import monkey; monkey.patch_socket()
from gevent.pool import Pool
from comarcas import insercao_vara
from gevent.threadpool import ThreadPool
import re
connect('processos')#, max_pool_size=500)
x = Advogado.objects.no_cache()
siglas = [u'AC', u'AL', u'AP', u'AM', u'BA', u'CE', u'DF',
          u'ES', u'GO', u'MA', u'MT', u'MS', u'MG', u'PA',
          u'PB', u'PR', u'PE', u'PI', u'RJ', u'RN', u'RS',
          u'RO', u'RR', u'SC', u'SP', u'SE', u'TO']
def limpando_latin_utf(string):
    return fix_bad_unicode(string.encode('UTF-8').decode('latin-1')).lower().strip()


def atualizar_advogado(advogado):
    advogado_atualizar_banco = {}
    advogado_update =Advogado.objects.get(_id=advogado.id)

    #numero_oab

    codigo_oab = advogado_update.oab

    if codigo_oab[0:2] in siglas:
        sigla = codigo_oab[0:2]
        numero = codigo_oab[2:]
        print "inteiro: ", codigo_oab, " estado: ", sigla, " numero: ", numero
        try:
            advogado_update.update(set__oab=numero,
                                   set__inscricao=sigla)
        except Exception as e:
            print "Erro: %s de sigla" % e
            advogado_update.delete()
            return False
    elif codigo_oab[0:2] not in siglas and codigo_oab[-1] == u'/':
        numero = codigo_oab.replace(u'/', u'').split()
        try:
            advogado_update.update(set__oab=numero)
        except:
            advogado_update.delete()
            return False
    #nome
    try:
        if len(limpando_latin_utf(advogado_update.nome).split(u' ')) == 1:
            print "deletando %s ObjectId('%s')" % (advogado_update.nome,  advogado_update.id)
            print codigo_oab
            advogado_update.update(set__nome=u'')
        else:
            adv = advogado_update.nome
            if adv[-1] == u'.':
                adv_limpo = limpando_latin_utf(advogado_update.nome).replace(u'.', u'')
                advogado_update.update(set__nome=adv_limpo)
            else:
                adv_merda = limpando_latin_utf(adv)
                advogado_update.update(set__nome=adv_merda)

    except Exception as e:
        print "Erro %s" % e, sys.exc_info()[2].tb_lineno

def main():
    count = 0
    for advogado in x:
        atualizar_advogado(advogado)
        # print count
        # count += 1

if __name__ == '__main__':
    main()