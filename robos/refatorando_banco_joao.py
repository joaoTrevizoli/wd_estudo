#!/usr/bin/env python
# encoding: utf-8
__author__ = 'joaotrevizoliesteves'

import sys
import unicodedata
import pymongo.errors
from models import *
from utilitarios import fix_bad_unicode
from datetime import datetime
from time import sleep
from hashlib import md5
from gevent import monkey; monkey.patch_socket()
from gevent.pool import Pool
from comarcas import insercao_vara
from gevent.threadpool import ThreadPool
import re
connect('processos')
x = Processos.objects.no_cache()

tribunal = {'01': u'TRT1',
            '02': u'TRT2',
            '03': u'TRT3',
            '04': u'TRT4',
            '05': u'TRT5',
            '06': u'TRT6',
            '07': u'TRT7',
            '08': u'TRT8',
            '09': u'TRT9',
            '10': u'TRT10',
            '11': u'TRT11',
            '12': u'TRT12',
            '13': u'TRT13',
            '14': u'TRT14',
            '15': u'TRT15',
            '16': u'TRT16',
            '17': u'TRT17',
            '18': u'TRT18',
            '19': u'TRT19',
            '20': u'TRT20',
            '21': u'TRT21',
            '22': u'TRT22',
            '23': u'TRT23',
            '24': u'TRT24'}

regiao = {'01': u'RJ',
          '02': u'SP - Capital',
          '03': u'MG',
          '04': u'RS',
          '05': u'BA',
          '06': u'PE',
          '07': u'CE',
          '08': u'PA/AP',
          '09': u'PR',
          '10': u'DF/TO',
          '11': u'AM/RR',
          '12': u'SC',
          '13': u'PB',
          '14': u'RO/AC',
          '15': u'SP - Interior',
          '16': u'MA',
          '17': u'ES',
          '18': u'GO',
          '19': u'AL',
          '20': u'SE',
          '21': u'RN',
          '22': u'PI',
          '23': u'MT',
          '24': u'MS'}


def atualizar_banco(processo):
    dados_atualizar_banco = {}
    processo_update = Processos.objects.get(_id=processo.id)
    numero_processo = re.compile('\d{7}\-\d{2}\.\d{4}\.\d\.\d{2}\.\d{4}')

    #Correcao do numero e ano do processo
    try:
        if numero_processo.match(processo_update.numero).group(0):
            pass
            # print numero_processo.match(processo_update.numero).group(0)
            # print 'obedece o padrao de numero'
    except:
        print 'nao obedece o padrao deletando registro'
        print processo_update.numero
        processo.delete()
        # sleep(3)
        return False
    try:
        ano_processo = processo_update.numero[11:15]
        regiao_processo = processo_update.numero[18:20]
        dados_atualizar_banco['ano_processo'] = unicode(ano_processo)
        dados_atualizar_banco['tribunal'] = unicode(tribunal[regiao_processo])
        dados_atualizar_banco['regiao'] = unicode(regiao[regiao_processo])
        try:
            dados_atualizar_banco['vara'] = insercao_vara(regiao_processo, processo_update.numero[-4:])
            processo_update.update(set__comarca=dados_atualizar_banco['vara'])
        except Exception as e:
            # print "Erro: ", e, sys.exc_info()[2].tb_lineno
            escrever = u"Vara: %s, tribunal: %s\n" % (e, processo_update.numero[18:])
            with open("varas_erradas", "a") as varas_erradas:
                varas_erradas.write(escrever.encode("UTF-8"))
    except Exception as e:
            print "Erro: ", e, sys.exc_info()[2].tb_lineno
    #classe
    try:
        if hasattr(processo_update, 'classe') and type(processo_update.classe) != type(None):
            dados_atualizar_banco['classe'] = (fix_bad_unicode(processo_update.classe.encode('UTF-8').decode('latin-1'))).lower()
        else:
            dados_atualizar_banco['classe'] = None
    except Exception as e:
        # print "Erro: ", e, sys.exc_info()[2].tb_lineno
        # print "sem classe"
        return False
    #Regiao
    # try:
    #     if type(processo_update.tribunal) != type(None):
    #         dados_atualizar_banco['regiao'] = regiao[processo_update.tribunal]
    #         print 'Deu certo regiao!!!'
    #     else:
    #         dados_atualizar_banco['regiao'] = None
    # except Exception as e:
    #     print "Erro: ", e, sys.exc_info()[2].tb_lineno


    #distribuicao
    try:
        try:
            dados_atualizar_banco['distribuicao'] = datetime.strptime(processo_update.distribuicao, '%d/%m/%Y %H:%M:%S')
        except:
            dados_atualizar_banco['distribuicao'] = processo_update.distribuicao
    except Exception as e:
        # print "Erro: ", e, sys.exc_info()[2].tb_lineno
        dados_atualizar_banco['distribuicao'] = None

    #juiz
    try:
        if hasattr(processo_update, 'juiz') and type(processo_update.juiz) != type(None):
            dados_atualizar_banco['juiz'] = fix_bad_unicode(processo_update.juiz.encode('UTF-8').decode('latin-1'))
        else:
            dados_atualizar_banco['juiz'] = None
    except Exception as e:
        print "Erro: ", e, sys.exc_info()[2].tb_lineno

    #area
    try:
        if hasattr(processo_update, 'area') and type(processo_update.area) != type(None):
            area = fix_bad_unicode(processo_update.area.encode('UTF-8').decode('latin-1'))

            if area[-1] == '.':
                 dados_atualizar_banco['area'] = area[0:1].lower()
            else:
                dados_atualizar_banco['area'] = area.lower()
    except Exception as e:
        # print "Erro: ", e, sys.exc_info()[2].tb_lineno
        dados_atualizar_banco['area'] = None

    #local_fisico
    try:
        if hasattr(processo_update, 'local_fisico') and type(processo_update.local_fisico) != type(None):
            local_fisico = fix_bad_unicode(processo_update.local_fisico.encode('UTF-8').decode('latin-1'))
            if local_fisico[-1] == '.':
                dados_atualizar_banco['local_fisico '] = local_fisico[0:-1]
            else:
                dados_atualizar_banco['local_fisico'] = local_fisico
        else:
            dados_atualizar_banco['local_fisico'] = None
    except Exception as e:
        print "Erro: ", e, sys.exc_info()[2].tb_lineno
        dados_atualizar_banco['local_fisico'] = None

    #Valor
    try:
        if hasattr(processo_update, 'valor') and type(processo_update.valor) != type(None):
            if processo_update.valor == Decimal(0) or processo_update.valor == Decimal(0.0):
                dados_atualizar_banco['valor'] = None
            else:
                dados_atualizar_banco['valor'] = processo_update.valor
        else:
            dados_atualizar_banco['valor'] = None
            # print 'sem valor passando reto'
    except Exception as e:
        print "Erro: ", e, sys.exc_info()[2].tb_lineno

    #Assunto
    try:
        if hasattr(processo_update, 'assunto') and type(processo_update.assunto) != type(None):
            novo_assunto = (fix_bad_unicode(processo_update.assunto.encode('UTF-8').decode('latin-1')))
            assunto = novo_assunto.lower()
            if assunto[-1] == '.':
                dados_atualizar_banco['assunto'] = assunto[0:-1]
            else:
                dados_atualizar_banco['assunto'] = assunto
        else:
            dados_atualizar_banco['assunto'] = None
    except Exception as e:
        # print "Erro: ", e, sys.exc_info()[2].tb_lineno
        print processo_update.url

    #origem
    try:
        if hasattr(processo_update, 'origem') and type(processo_update.origem) != type(None):
            if processo_update.origem[0] == '(' and processo_update.origem[-1] == ')':
                #print processo_update.origem[1:-1].strip()
                dados_atualizar_banco['origem'] = (fix_bad_unicode(processo_update.origem[1:-1].strip().encode('UTF-8').decode('latin-1'))).replace(u'ª', u'')
                if dados_atualizar_banco['origem'][-1] == '.':
                    dados_atualizar_banco['origem'] = dados_atualizar_banco['origem'][0:-1]
            else:
                dados_atualizar_banco['origem'] = (fix_bad_unicode(processo_update.origem.encode('UTF-8').decode('latin-1'))).replace(u'ª', u'')
            # print dados_atualizar_banco['origem']
        else:
            dados_atualizar_banco['origem'] = None
    except Exception as e:
        dados_atualizar_banco['origem'] = None
        # print "Erro: ", e, sys.exc_info()[2].tb_lineno
        # print processo_update.id
    # print dados_atualizar_banco['origem']
    #partes
    partes = processo.partes
    partes_novas = []
    try:
        for parte in partes:
            parte_atualizada = {}
            nome = fix_bad_unicode(parte.nome.encode('UTF-8').decode('latin-1')).lower()
            if nome[-1] == '.':
                parte_atualizada['nome'] = unicodedata.normalize('NFKD', nome[0:-1]).encode('ASCII', 'ignore')
            else:
                parte_atualizada['nome'] = unicodedata.normalize('NFKD', nome).encode('ASCII', 'ignore')
            # print fix_bad_unicode(parte.nome.lower())
            if type(parte.advogados) == unicode or type(parte.advogados) == str:
                string_a_limpar =fix_bad_unicode(parte.advogados.encode('UTF-8').decode('latin-1'))
                if string_a_limpar != '':
                    adv_update = []
                    lista_advogado = string_a_limpar.replace('ADVOGADO: ', '').lower().split(',')
                    for advogado in lista_advogado:
                        if len(advogado.strip()) > 3:
                            adv_update.append(advogado.strip())
                            try:
                                # print "tentando"
                                adv = Advogado.objects.get(nome=advogado.strip())
                                # print adv.nome
                                adv.update(add_to_set__processos=processo_update)
                                # print("adicionada referencia")
                            except:
                                escrever = "%s, %s,\n" % (advogado.strip(), dados_atualizar_banco['regiao'])
                                with open("adv_n_encontrado", "a+") as droga:
                                    droga.write(escrever.encode("UTF-8"))
                                pass
                    parte_atualizada['advogados'] = adv_update
                else:
                    parte_atualizada['advogados'] = None
            else:
                parte_atualizada['advogados'] = parte.advogados
            if parte.tipo[-4:] == '(S):' or parte.tipo[-4:] == '(s):':
                tipo = fix_bad_unicode(parte.tipo.encode('UTF-8').decode('latin-1'))
                parte_atualizada['tipo'] = unicodedata.normalize('NFKD', tipo.lower().replace('(s):', '')).encode('ASCII', 'ignore')
            else:
                parte_atualizada['tipo'] = unicodedata.normalize('NFKD',fix_bad_unicode(parte.tipo.encode('UTF-8').decode('latin-1'))).encode('ASCII', 'ignore')
            partes_novas.append(parte_atualizada)
        dados_atualizar_banco['partes'] = [Partes(nome=parte['nome'], tipo=parte['tipo'], advogados=parte['advogados']) for parte in partes_novas]
    except Exception as e:
        # print "Erro: ", e, sys.exc_info()[2].tb_lineno
        dados_atualizar_banco['partes'] = []

    #movimentacoes
    movimentocoes_novas = []
    try:
        for movimentacao in processo.movimentacoes:
            movimentacoes = {}
            data = movimentacao.data
            try:
                try:
                    movimentacoes['data'] = datetime.strptime(data, '%d/%m/%Y %H:%M:%S')
                except:
                    movimentacoes['data'] = datetime.strptime(data, '%d/%m/%Y')
            except:
                movimentacoes['data'] = data
            movi = fix_bad_unicode(movimentacao.descricao.encode('UTF-8').decode('latin-1'))
            try:
                if movi[0] == '"' and movi[-1] == '"':
                    movi_sem_aspas = movi[1:-1]
                    if movi_sem_aspas[-1] == '.':
                        movimentacoes['descricao'] = movi_sem_aspas[0:-1].replace(': ', '').replace('#', '').replace('|', '').replace('(documento restrito)', '').strip()
                    else:
                        movimentacoes['descricao'] = movi_sem_aspas.replace(': ', ' ').replace('#', '').replace('|', '').replace('(documento restrito)', '').strip()
                else:
                    if movi[-1] == '.':
                        movimentacoes['descricao'] = movi[0:-1].replace(': ', '').replace('#', '').replace('|', '').replace('(documento restrito)', '').strip()
                    else:
                        movimentacoes['descricao'] = movi.replace(': ', ' ').replace('#', '').replace('|', '').replace('(documento restrito)', '').strip()
                movimentocoes_novas.append(movimentacoes)
            except:
                pass
        if len(movimentocoes_novas):
            dados_atualizar_banco['movimentacoes'] = [Movimentacoes(data=data['data'], descricao=data['descricao']) for data in
                                          movimentocoes_novas]
            # print u'hasheando a movimentaçao: ', movimentocoes_novas[0]['descricao']
            dados_atualizar_banco['ultima_movimentacao'] = md5(movimentocoes_novas[0]['descricao'].encode('UTF-8')).hexdigest()
        else:
            dados_atualizar_banco['movimentacoes'] = None
            dados_atualizar_banco['ultima_movimentacao'] = None
    except Exception as e:
        pass
        # print "Erro: ", e, sys.exc_info()[2].tb_lineno

    try:
        processo_update.update(set__regiao=dados_atualizar_banco['regiao'],
                               set__ano=dados_atualizar_banco['ano_processo'],
                               set__tribunal=dados_atualizar_banco['tribunal'],
                               set__movimentacoes=dados_atualizar_banco['movimentacoes'],
                               set__juiz=dados_atualizar_banco['juiz'],
                               set__assunto=dados_atualizar_banco['assunto'],
                               set__area=dados_atualizar_banco['area'],
                               set__partes=dados_atualizar_banco['partes'],
                               set__origem=dados_atualizar_banco['origem'],
                               set__local_fisico=dados_atualizar_banco['local_fisico'],
                               set__distribuicao=dados_atualizar_banco['distribuicao'],
                               set__valor=dados_atualizar_banco['valor'],
                               set__ultima_movimentacao=dados_atualizar_banco['ultima_movimentacao']
                               )
        try:
            if processo_update.atualizado_em <= datetime.strptime('20/11/2014', '%d/%m/%Y'):
                processo_update.update(set__tipo_processo='PJE')
                # print 'adicionado o tipo de processo PJE'
            else:
                pass
                # print "sem data"
                # sleep(5)
        except:
            print Exception, sys.exc_info()[2].tb_lineno



    except Exception as e:
        # sleep(10)
        print 'Erro: ', e, sys.exc_info()[2].tb_lineno
        return False
    try:
        processo_update.update(set__classe=dados_atualizar_banco['classe'])
    except Exception as e:
        # print 'Erro: ', e, sys.exc_info()[2].tb_lineno
        deletar = Processos.objects(numero=processo_update.numero)
        for dado_deletar in deletar:
            try:
                if len(dado_deletar.movimentacoes) == 0:
                    dado_deletar.delete()
                    processo_update.update(set__classe=dados_atualizar_banco['classe'])
                    # print "removido Duplicado"
            except Exception as e:
                print "erro do caralho: ", e

        return False

def main():
    count = 0
    pool = Pool(500)
    for processo in x:
        pool.spawn(atualizar_banco, processo)
        # atualizar_banco(processo)
        count += 1
        print(count)


if __name__ == "__main__":
    main()