#!/usr/bin/env python
# encoding: utf-8

"""
Essa classe sera responsavel por instanciar
requests ja com o proxy.

Lab804 http://www.lab804.com.br - 2014

http://www.proxy4free.com/list/webproxy1.html           (Free)
http://www.samair.ru/proxy-by-country/Brazil-01.htm     (Pago)
https://www.hidemyass.com/servers                       (Pago)
http://kingproxies.com                                  (Pago)

HOST TIPO PAIS CHECADO, USADO

"""

__author__ = 'murilobsd'

import requests as requesocks
from lxml import html
import time
from urllib import urlopen
import re

class Proxy(object):
    def __init__(self, ip, porta, tipo):
        self.ip = ip
        self.porta = porta
        self.tipo = tipo

    def __str__(self):
        return '"{0}": "{1}:{2}"'.format(self.tipo, self.ip, self.porta)

    def __repr__(self):
        return "<Proxy {0}:{1}>".format(self.ip,self.porta)

class ZombieProxy(object):
    proxy_list = []

    url = 'http://www.samair.ru/proxy/'

    sessao = requesocks.Session()

    ip = re.compile(r'Address:\s(\d+\.\d+\.\d+\.\d+)', re.UNICODE)

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0'
    }

    portas = {'l': '6',
              'n': '1',
              'o': '8',
              'p': '5',
              'q': '3',
              's': '9',
              'u': '0',
              'v': '7',
              'x': '2',
              'y': '4'}

    def adicionar(self, valor):
        self.proxy_list.append(valor)

    def get(self, tipo=None):
        pass

    def get_ip(self, sessao):
        data = sessao.get('http://checkip.dyndns.com/', headers=self.headers).content
        try:
            ip = self.ip.findall(data)[0]
        except:
            ip = ""
        return ip

    def coletar_proxys(self):
        subs = {}
        site = self.sessao.get(self.url, headers=self.headers)
        if site.status_code == 200:
            print site.content
            #root = html.fromstring(site.content)
            for t in re.findall(u'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}).*?\:\"((\+[a-z])+)+(<td>)', site.content):
                print t
            #paginacao = 1
            #root = html.fromstring(site.content)
            #proxima = root.xpath('.//*[@id="navbuttons"]/a[contains(text(), "next")]')
            #while proxima:
            #    print "Pagina: %d" % paginacao
            #    linhas_proxy = root.xpath('.//*[@id="proxylist"]/tr[position()>1]')
            #    for linha in linhas_proxy:
            #        # host, situacao, data, hora, pais = linha.xpath('./td/text()')
            #        port = linha.xpath('./td[1]/script')
            #        print port
                    # if port in ['3128', '8080', '80', '3127', '8118']:
                    #    tipo = 'http'
                    #    print host, port

           #     proxima = root.xpath('.//*[@id="navbuttons"]/a[contains(text(), "next")]')
           #     paginacao += 1
           #     time.sleep(1.5)



print ZombieProxy().coletar_proxys()