#!/usr/bin/env python
# encoding: utf-8

"""

Modulo responsavel para tratar e buscar as informacoes do tribunal
de justica do estado de sao paulo.

Lab804 http://www.lab804.com.br - 2014

"""

__author__ = 'murilobsd'

import requesocks
import requests
from lxml import html
import re
import time
#from models import Movimentacoes, Advogado
import erequests

class RoboTJSP(object):
    """
    Classe Responsavel para instanciar robos para o tribunal de justica do
    estado de sao paulo.
    """
    # Expressao Regular para Casar: Nome (OAB)
    cavucar_adv = re.compile(r'(((?:\s\w+)+)\s\(OAB\s(\d+\/\w{2})\))')

    # Match
    numero_oab = re.compile(r'\d+\/[A-Z]{2}')

    # Cabecalho
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0'
    }

    # Dicionario com as possiveis urls para a busca de dados..
    urls = {'oab':'http://esaj.tjsp.jus.br/cpo/pg/search.do;' \
                    'jsessionid=CD111D4A3343A94F6D4E129A4FDB26A0.cpo4?' \
                    'conversationId=&paginaConsulta=1&localPesquisa.cdLocal=-1' \
                    '&cbPesquisa=NUMOAB&tipoNuProcesso=UNIFICADO&' \
                    'dePesquisa={0}',
            'nome': '',
            'processo': '',
            'base': 'http://esaj.tjsp.jus.br',
            'paginacao': 'http://esaj.tjsp.jus.br/cpo/pg/'
    }

    tribunal = 'TJSP'

    def __init__(self, proxy_tor=False, timeout=5, limite=3, tempo_requisicao=0.5, async=False):
        """
        Iniciando o RoboTJSP
        :param proxy_tor: Boolean
        :param timeout: Inteiro
        :param limite: Inteiro
        :return:
        """
        if proxy_tor:
            self.proxy = True
            self.sessao = requesocks
            self.sessao.proxies = {'http': 'socks5://127.0.0.1:9050'}
            print "Utilizando o Tor."
        else:
            self.proxy = False
            self.sessao = requests
        self.erros = 0                              #  Tentativas de Conexao.
        self.resultado = []                         # Resultado da pesquisa.
        self.novos = []                             # Novos Advogados encontrados.
        self.timeout = int(timeout)                 # Quantos segundos esperar para o request dar timeout.
        self.limite = int(limite)                   # Numero de Tentativas de conectar antes de usar o proxy.
        self.tempo_req = float(tempo_requisicao)
        self.async = async                    # Se ativando usa requsicoes async.
        self.advogados = set()
        self.urls_403 = set()

    @property
    def ativar_proxy(self):
        if not self.proxy:
            self.sessao = requesocks
            self.sessao.proxies = {'http': 'socks5://127.0.0.1:9050'}
        else:
            print u"Precisa ser feita a reconexao."
            return

    def _ajustar_oab(self, numero):
        if numero:
            try:
                self.numero_oab.match(numero).group(0)
                num = numero.replace('/', '')
                return num
            except:
                return False
        else:
            return False

    def cade_adv(self, texto):
        advogados = self.cavucar_adv.findall(texto)
        for advogado_completo, nome, oab in advogados:
            print self._ajustar_oab(oab)
            self.advogados.add(oab)

        """
        if advogados:
            for advogado_completo, nome, oab in advogados:
                try:
                    sufixo = oab[-2:]
                    prefixo = oab[:-2]
                    Advogado.objects.create(nome=nome, inscricao=sufixo, oab=prefixo)
                    print "Adicionando Novo Advogado para pesquisa. %s" % nome
                except:
                    pass
        else:
            return False
        """

    def limpeza(self, palavra):
        """
        limpa algumas sujeiras dos tribunais.
        :param palavra: string
        :return: string
        """
        if palavra:
            return re.sub(r'\s+', ' ', palavra).strip()
        else:
            return False

    def lista_processos(self, conteudo):
        """
        Funcao para listar os links de processos contido no site de pesquisa.
        :param conteudo: texto html para pesquisar por link de processos.
        :return: array contendo as listas.
        """
        if conteudo:
            miolo = html.fromstring(conteudo)
            links = miolo.xpath('//a[@class="linkProcesso"]/@href')
            return links
        else:
            return False

    def get(self, url, headers=None):
        """
        Retorna o method get em determinada url.
        """
        tentativas = 0
        if not headers:
            headers = self.headers
        while self.erros <= self.limite:
            try:
                return self.sessao.get(url, headers=headers, timeout=self.timeout)
            except Exception as e:
                print "Erro ao conectar: %s - %s" % (url, e)
                self.erros += 1

        # Atingiu o numero maximo de tentativas, vamos ativar o proxy.
        if self.erros >= self.limite and tentativas < 5:
            print "Ativando o proxy, após %d tentativas." % self.limite
            while tentativas < 5:
                try:
                    self.ativar_proxy
                    return self.sessao.get(url, headers=headers, timeout=self.timeout)
                except Exception as e:
                    print "Erro ao conectar: %s - %s descartando url." % (url, e)
                    tentativas += 1
        else:
            return False

    def extrair(self, conteudo, url):
        resultado = {}
        partes_conteudo = ""
        miolo = html.fromstring(conteudo)
        try:
            processo = miolo.xpath('//td/span[@class=""][1]/text()')[0]
        except Exception as e:
            print "Se não possui processo não tem o porque continuar."
            self.erros += 1
            return False
        area = miolo.xpath('//td/span[@class="labelClass"]/../text()')[1].strip()
        classe_processo = miolo.xpath('//td/span/span/text()')[0]

        try:
            assunto = miolo.xpath('//tr/td/div[contains(text(), "Assunto")]/../../td/span/text()')[0]
        except:
            assunto = ""
        distribuicao = miolo.xpath('//tr/td/div[contains(text(), "Distribui")]/../../td/span/text()')[0]
        try:
            origem = \
                miolo.xpath('//tr/td/div[contains(text(), "Distribui")]/ancestor::tr/following::tr/td/span/text()')[0]
        except:
            origem = False
        try:
            local = miolo.xpath('//tr/td/div[contains(text(), "Local")]/../../td/span/text()')[0]
        except:
            local = ""
        try:
            juiz = miolo.xpath('//tr/td/div[contains(text(), "Juiz")]/../../td/span/text()')[0]
        except:
            juiz = ""
        try:
            valor = miolo.xpath('//tr/td/div[contains(text(), "Valor")]/../../td/span/text()')[0]
        except:
            valor = ""
        linhas = miolo.xpath('//table[contains(@id, "tabelaTodasMovimentacoes")]/tr')

        # Se a tabelaTodasPartes existe otimo.
        if miolo.xpath('.//table[@id="tableTodasPartes"]/tr'):
            linhas2 = miolo.xpath('.//table[@id="tableTodasPartes"]/tr')
            partes = []
            for linha in linhas2:
                partes.append((self.limpeza(linha.xpath('./td[1]/span[@class="mensagemExibindo"]/text()')[0]),
                               self.limpeza(" ".join(linha.xpath('./td[2]//text()')))))
        else:
            linhas2 = miolo.xpath('.//table[@id="tablePartesPrincipais"]/tr')
            partes = []
            for linha in linhas2:
                partes.append((self.limpeza(linha.xpath('./td[1]/span/text()')[0]),
                               self.limpeza(" ".join(linha.xpath('./td[2]//text()')))))

        movimentacoes = []

        for linha in linhas:
            data = self.limpeza(linha.xpath('td[1]/text()')[0])
            descricao = self.limpeza(" ".join(linha.xpath('td[3]//text()')))
            movimentacoes.append((data, descricao))
            self.cade_adv(descricao)  # Procurando por advogados e adicionando na variavel global adv.

        resultado['tribunal'] = self.tribunal
        resultado['url'] = url
        resultado['numero'] = self.limpeza(processo)
        resultado['area'] = area
        resultado['classe'] = classe_processo
        resultado['assunto'] = assunto
        resultado['distribuicao'] = distribuicao
        resultado['local_fisico'] = local
        resultado['juiz'] = juiz
        resultado['valor'] = valor
        resultado['orgao_origem'] = origem
        for parte in partes:
            partes_conteudo += " " + " ".join(parte)
        resultado['partes'] = partes_conteudo
        resultado['movimentacoes'] = [(data, dados) for data, dados in movimentacoes]
        return resultado

    def coleta_dados_async(self, req):
        """
        Coletar dados apos requisicao async.
        :param req:
        :return:
        """
        erros = []
        if req:
            requisicoes = req
            for requisicao in requisicoes:
                if requisicao.status_code == 200:
                    self.resultado.append(self.extrair(requisicao.content, requisicao.url))
                else:
                    erros.append({'url': requisicao.url})
                if erros:
                    for erro in erros:
                        self.resultado.append(self.extrair(self.get(erro['url']).content, erro['url']))
        else:
            return False

    def coleta_dados(self, url, tribunal):
        """
        Funcao para coletar os possiveis dados existentes na url do processo.
        :param url: url do processo.
        :param tribunal: sigla do tribunal
        :return: dict com os dados coletados.
        """

        resultado = {}
        site = self.get(url)

        if site.status_code == 200:
            conteudo = site.content

            partes_conteudo = ""

            miolo = html.fromstring(conteudo)
            processo = miolo.xpath('//td/span[@class=""][1]/text()')[0]
            area = miolo.xpath('//td/span[@class="labelClass"]/../text()')[1].strip()
            classe_processo = miolo.xpath('//td/span/span/text()')[0]

            try:
                assunto = miolo.xpath('//tr/td/div[contains(text(), "Assunto")]/../../td/span/text()')[0]
            except:
                assunto = ""
            distribuicao = miolo.xpath('//tr/td/div[contains(text(), "Distribui")]/../../td/span/text()')[0]
            try:
                origem = \
                    miolo.xpath('//tr/td/div[contains(text(), "Distribui")]/ancestor::tr/following::tr/td/span/text()')[0]
            except:
                origem = False
            try:
                local = miolo.xpath('//tr/td/div[contains(text(), "Local")]/../../td/span/text()')[0]
            except:
                local = ""
            try:
                juiz = miolo.xpath('//tr/td/div[contains(text(), "Juiz")]/../../td/span/text()')[0]
            except:
                juiz = ""
            try:
                valor = miolo.xpath('//tr/td/div[contains(text(), "Valor")]/../../td/span/text()')[0]
            except:
                valor = ""
            linhas = miolo.xpath('//table[contains(@id, "tabelaTodasMovimentacoes")]/tr')

            # Se a tabelaTodasPartes existe otimo.
            if miolo.xpath('.//table[@id="tableTodasPartes"]/tr'):
                linhas2 = miolo.xpath('.//table[@id="tableTodasPartes"]/tr')
                partes = []
                for linha in linhas2:
                    partes.append((self.limpeza(linha.xpath('./td[1]/span[@class="mensagemExibindo"]/text()')[0]),
                                   self.limpeza(" ".join(linha.xpath('./td[2]//text()')))))
            else:
                linhas2 = miolo.xpath('.//table[@id="tablePartesPrincipais"]/tr')
                partes = []
                for linha in linhas2:
                    partes.append((self.limpeza(linha.xpath('./td[1]/span/text()')[0]),
                                   self.limpeza(" ".join(linha.xpath('./td[2]//text()')))))

            movimentacoes = []

            for linha in linhas:
                data = self.limpeza(linha.xpath('td[1]/text()')[0])
                descricao = self.limpeza(" ".join(linha.xpath('td[3]//text()')))
                movimentacoes.append((data, descricao))
                self.cade_adv(descricao)  # Procurando por advogados e adicionando na variavel global adv.

            resultado['tribunal'] = tribunal
            resultado['url'] = url
            resultado['numero'] = self.limpeza(processo)
            resultado['area'] = area
            resultado['classe'] = classe_processo
            resultado['assunto'] = assunto
            resultado['distribuicao'] = distribuicao
            resultado['local_fisico'] = local
            resultado['juiz'] = juiz
            resultado['valor'] = valor
            resultado['orgao_origem'] = origem
            for parte in partes:
                partes_conteudo += " " + " ".join(parte)
            resultado['partes'] = partes_conteudo
            resultado['movimentacoes'] = [(data, dados) for data, dados in movimentacoes]
            return resultado
        else:
            print u"Erro no status da url: %s - %s" % (site.status_code, url)
            return False

    def buscar(self, oab=False, nome=False, processo=False):
        """
        Funcao para buscar informacoes no tribunal.
        :param oab: numero da oab mais estado
        :param nome: nome do advogado
        :param processo: numero do processo.
        :return: None
        """

        if oab:
            url = self.urls['oab']
        else:
            url = None

        # Sera que possui paginacao.
        fimPaginacao = True  # Somente para entrar no loop e verificar a primeira pagina.
        proxPagina = "" # Se tiver proximas paginas serao preenchidas.
        paginacao = 1
        while fimPaginacao:
            if proxPagina != "":
                site = self.get(self.urls['paginacao'] + proxPagina)
            else:
                site = self.get(url.format(oab))

            if site.status_code == 200:
                print u"Coletando processos da página: [%s] - [%d]" % (oab, paginacao)
                conteudo = site.content
                miolo = html.fromstring(conteudo)
                links = self.lista_processos(conteudo)

                if links and self.async:
                    req = [erequests.async.get(self.urls['base'] + link, session=self.sessao,
                                               headers=self.headers, timeout=self.timeout) for link in links]
                    requisicoes = list(erequests.map(req))
                    self.coleta_dados_async(requisicoes)

                elif links:
                    for link in links:
                        self.resultado.append(self.coleta_dados(self.urls['base'] + link, 'TJSP'))
                        time.sleep(self.tempo_req)
                else:
                    print u"Não possui mais links para paginacao: %s" % self.urls['paginacao'] + proxPagina
                    return False

                # proximo link.

                try:
                    proxPagina = miolo.xpath('//a[contains(text(), ">")]/@href')[0]
                except IndexError:
                    proxPagina = ""

                fimPaginacao = miolo.xpath('//a[contains(text(), ">")]')
                paginacao += 1
            else:
                if site.status_code == 403:
                    self.urls_403.add(403)
                print "Erro no status code: %s" % site.status_code

            if proxPagina == "":
                print u"Não foram mais encontrados páginações para [%s]." % oab
                fimPaginacao = False

    @property
    def _quantidade(self):
        return len(self.resultado)


