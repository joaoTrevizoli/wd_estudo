# encoding: utf-8

__author__ = 'murilobsd'

import requesocks
import re
import os
from lxml import html

class DiariosTRT(object):

    url = 'http://www.jusbrasil.com.br'
    url_diarios = 'http://www.jusbrasil.com.br/diarios/'

    # http://www.jusbrasil.com.br/diarios/TRT-1/2014/11/27/JUDICIARIO?view_page=2

    headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0',
            'Content-Type': 'text/html; application/x-www-form-urlencoded; charset=UTF-8'
    }


    def acessar_url(self, url, method=('GET',)):
        """
        Funcao para acessar as urls
        :param url: str
        :return: object response
        """
        sessao = requesocks.session()
        sessao.proxies = {'http': 'socks5://127.0.0.1:9050'}

        if 'GET' in method:
            try:
                conteudo = sessao.get(url, headers=self.headers)
                return conteudo
            except:
                pass

    def extrair_links_trt(self, conteudo):
        """
        Funcao que extrai os links de cada TRT.
        :param conteudo: obj response
        :return:str
        """
        if conteudo:
            root = html.fromstring(conteudo.content)
            try:
                return root.xpath('//a[contains(@href, "TRT-")]/@href')
            except Exception as e:
                print "Erro ao extrair os links dos tribunais: %s" % e
                return False
        else:
            return False

    def coletar_links_trt(self):
        """
        Funcao que acessa a url principal onde estao os links para os tribunais..
        :return: list
        """
        conteudo = self.acessar_url(url=self.url_diarios)
        if conteudo.status_code == 200:
            links = self.extrair_links_trt(conteudo)
            if links:
                self.links_tribunais = [self.url+link for link in links]
            else:
                print "Não foram encontrados links para url: %s" % self.url_diarios
        else:
            print "Status Code != 200, na coleta de links dos tribunais do trabalho no JusBrasil: %s" % \
                  conteudo.status_code
            return False

    def extrair_diarios(self):
        pass



class DiariosJus(object):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0',
        'Content-Type': 'text/html; application/x-www-form-urlencoded; charset=UTF-8'
    }
    url = "http://www.jusbrasil.com.br/diarios/81215699/djsp-judicial-1a-instancia-interior-parte-ii-28-11-2014-pg-1"

    def acessar_url(self, url, method=('GET',)):
        """
        Funcao para acessar as urls
        :param url: str
        :return: object response
        """
        sessao = requesocks.session()
        sessao.proxies = {'http': 'socks5://127.0.0.1:9050'}

        if 'GET' in method:
            try:
                conteudo = sessao.get(url, headers=self.headers)
                return conteudo
            except:
                pass

    def extrair_dados(self):
        #conteudo = self.acessar_url(url=self.url)
        arquivo = open('/home/murilobsd/projetos/wd/robos/htmls/dj_jusbrasil.html', 'r')
        linhas = "".join(arquivo.readlines()).strip()
        arquivo.close()
        root = html.fromstring(linhas)
        dados = u"".join(root.xpath('//*[@id="wrap"]/section/section/section/article/p/text()')).strip()
        processos = re.compile(r'((.*)PROCESSO)')
        print "\n"
        print dados
        print "\n\n"
        print processos.findall(dados)

if __name__ == '__main__':
    #DiariosTRT().coletar_links_trt()
    DiariosJus().extrair_dados()
