#!/usr/bin/env python
# -*- coding: utf-8 -*-

from argparse import ArgumentParser
from datetime import datetime, timedelta
import requesocks
from lxml import html
import re
import time
import os
import codecs
from multiprocessing import Process, Manager


# http://www.jusbrasil.com.br/diarios/TRT-2/2014/11/13/JUDICIARIO?view_page=4

advogados = re.compile((r'(\d+\/[A-Z]{2}\s\-(?:\s\w+)+)'), re.UNICODE)
paginacao = re.compile(r'\s\/\s\d+\s')
numero_processo = re.compile(r'.*(\d).*\s(\w+)\-(\d{7}\-\d+\.\d{4}\.\d\.\d{2}\.\d{4})')

headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0'
}

grouper_it = lambda ulist, step:  map(lambda i: ulist[i:i+step],  xrange(0, len(ulist), step))


def escrever(conteudo, arquivo):
    with codecs.open(arquivo, 'w', 'utf-8') as file:
        for item in conteudo:
            for x in item:
                try:
                    file.write(u"{}\n".format(x.strip()))
                except:
                    file.write(u"{}\n".format(x))

def dias_da_semana(data_inicio, data_final, proibido=set([5, 6])):
    dias = []
    dia = timedelta(days=1)
    if data_inicio and data_final:
        if data_final >= data_inicio:
            while data_inicio <= data_final:
                if data_inicio.weekday() not in proibido:
                    dias.append({'dia': "%02d"%data_inicio.day, 'mes': "%02d"%data_inicio.month,
                                 'ano': "%02d"%data_inicio.year})
                data_inicio = data_inicio + dia
            return dias
        else:
            print "Data Final menor do que a inicial"
            return False
    else:
        return False

def _extract_html(num, r, url, valor):
    dados = u" ".join(html.fromstring(r.get(url, headers=headers).content).xpath('/html/body/div[4]'
                                                                                 '/section/section/section'
                                                                                 '/article/p/text()'))
    if dados:
        possiveis_adv = advogados.findall(dados)
        if possiveis_adv:
            adv = set([possiveis.strip() for possiveis in possiveis_adv])
            valor['Processo: %d' % num] = adv
            return valor

def _advogados(sigla, caderno, ano, mes, dia, pags, r, chunk=10):
    # Lista de Advogados
    advogados = []
    manager = Manager()
    return_dict = manager.dict()

    # Lista de Urls que serao criadas.
    urls = []

    # Gerar quantidade de urls com relacao as paginas.
    url = "http://www.jusbrasil.com.br/diarios/{sigla}/{ano}/{mes}/{dia}/{caderno}?view_page={pagina}"
    for x in range(1, pags+1):
        urls.append(url.format(sigla=sigla, ano=ano, mes=mes, dia=dia, caderno=caderno, pagina=str(x)))

    # Chunks padrao de url 10
    chunks = grouper_it(urls, chunk)
    num = 0

    print "Quantidade de Chunks: %d" % len(chunks)

    while num < len(chunks):

        print "Faltam %d chunks: " % (len(chunks) - num)

        # 10 urls por vez
        processos = [Process(target=_extract_html, args=(x, r, c, return_dict)) for x, c in enumerate(chunks[num])]

        # Iniciando
        for p in processos:
            p.start()

        inicio = time.time()
        # Esperando Terminar o legal seria colocar um timeout
        for p in processos:
            p.join()

        fim = (time.time() - inicio) * 1000.0
        num += 1

        advogados.append(return_dict.values())
        print "Tempo: %0.3f ms para %d Urls." % (fim, chunk)
    return advogados


def quantidade_paginas(sigla, caderno, ano, mes, dia):

    if sigla and caderno and ano and mes and dia:
        url = "http://www.jusbrasil.com.br/diarios/{sigla}/{ano}/{mes}/{dia}/{caderno}?view_page={pagina}"
        url_consulta = url.format(sigla=sigla, ano=ano, mes=mes, dia=dia, caderno=caderno, pagina=str(1))
        session = requesocks.session()
        #session.proxies = {'http': 'socks5://127.0.0.1:9050'}
        paginas = session.get(url_consulta, headers=headers).content
        try:
            paginas = int(re.sub(r'[^\d+]', "", paginacao.findall(paginas)[0]))
        except:
            paginas = None
            pass

        if paginas:
            adv = _advogados(sigla, caderno, ano, mes, dia, paginas, session, 15)
            return adv
    else:
        return False


def main(pasta, sigla='DJSP', caderno='JUDICIAL_1A_INSTANCIA_CAPITAL', data_inicio=datetime(2008, 1, 1), data_final=datetime(2014, 10, 27)):

    dias = dias_da_semana(data_inicio, data_final)

    if dias:
        print "Quantidade de Dias para Pesquisar: ", len(dias)

        for dia in dias:
            print "Data: %s/%s/%s" % (dia['dia'], dia['mes'], dia['ano'])
            adv = quantidade_paginas('DJSP', caderno, dia['ano'], dia['mes'], dia['dia'])
            if adv:
                nome_arquivo = "{sigla}_{caderno}_{ano}_{mes}_{dia}.txt".format(sigla=sigla.lower(),
                                                                                 ano=dia['ano'].lower(),
                                                                                 mes=dia['mes'].lower(),
                                                                                 dia=dia['dia'].lower(),
                                                                                 caderno=caderno.lower())
                escrever(adv, os.path.join(pasta, nome_arquivo))


if __name__ == '__main__':
    parser = ArgumentParser()

    # Add more options if you like
    parser.add_argument("-di", "--datai",
                        help="Data Inicial", default=True)

    parser.add_argument("-df", "--dataf",
                        default=True,
                        help="Data Final")

    parser.add_argument("-p", "--pasta",
                        default=True,
                        help="Pasta onde serao salvos os arquivo")
    args = parser.parse_args()
    #try:
    datainicial = datetime.strptime(args.datai, '%d/%m/%Y')
    datafinal = datetime.strptime(args.dataf, '%d/%m/%Y')
    main(data_inicio=datainicial,
        data_final=datafinal,
        pasta=args.pasta)
    #except Exception as e:
    #    print "Erro: ", e
    #    pass
