#!/usr/bin/env python
# encoding: utf-8

__author__ = 'mazza'

"""
Segundo Crawler Mazza - 07/11/2014
TJRJ
"""

import re
import requests
from lxml import html


url_pesquisa = 'http://www4.tjrj.jus.br/consultaProcessoWebV2/consultaProc.do?v=2&numProcesso=2010.001.231545-2&FLAGNOME=N&tipoConsulta=publica&back=1&PORTAL=1&v=2'

# LIMPA A PARADA TODA
def limpeza(palavra):
    if palavra:
        return re.sub(r'\s+', ' ', palavra).strip()
    else:
        print u"Não temos a palavra"
        return False

def coleta_dados(url):

    try:
        site = requests.get(url_pesquisa)
    except Exception as e:
        site = None
        print "Erro: ", e

    if site.status_code == 200:
        conteudo = site.content
    else:
        print "Erro: %s" % site.status_code
        conteudo = False

    if conteudo:
        miolo = html.fromstring(conteudo)


        num_processo = miolo.xpath('//body//input[@name = "codigoProc"]/@value')[0]
        distribuicao = miolo.xpath('//form[@name = "formResultado"]//tr[5]/td/text()')[0]
        # limpadistribuicao = re.findall(u'Distribuído em\s+(\d+\/\d+\/\d+)',distribuicao)[0]
        orgao_origem = miolo.xpath('//td[@class = "info"][5]/text() | //td[@class = "info"][6]/text() | //td[@class = "info"][7]/text()')
        assunto = miolo.xpath('//td[contains(text(), "Assunto:")]/following-sibling::td/text()')[0]
        classe = miolo.xpath('//td[contains(text(), "Classe:")]/following-sibling::td/text()')[0].strip()
        autor = miolo.xpath('//td[contains(text(), "Autor")]/following-sibling::td/text()')[0].strip()
        reu = miolo.xpath(u'//td[contains(text(), "Réu")]/following-sibling::td/text()')[0].strip()
        advogados = miolo.xpath(u'//td[contains(text(), "Advogado(s):")]/following-sibling::td//text()')
        movimentacoes = miolo.xpath('//tr[@class = "tipoMovimento"]//tr//td//text()')

        print u"Nº Processo: " + num_processo
        print u"Órgão Origem: " + limpeza(" - ".join(orgao_origem))
        # print u"Distribuição: " + limpadistribuicao
        print u"Assunto: " + limpeza(assunto)
        print u"Classe: " + classe
        print u"Autor: " + autor
        print u"Réu: " + reu
        print " ".join(advogados).split()
        print movimentacoes


    else:
        print u"Sem conteúdo"
        return False

coleta_dados(url_pesquisa)

#
# rever = range(1, 190000, 200)
# # rever.reverse()
# for x in rever:
#     url_pesquisa = 'http://www.oab-rj.org.br/resultado_busca_inscritos.php?tipo=insc&nome=%d' % x
#     for adv in coleta_dados(url_pesquisa):
#         print adv

