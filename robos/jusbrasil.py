#!/usr/bin/env python
# encoding: utf-8

__author__ = 'joaotrevizoliesteves'

import re
import requesocks
from models import Processos
from datetime import timedelta, datetime

url_base = "http://www.jusbrasil.com.br/diarios/documentos/TRT-{0}/{1}/{2}/{3}/Judiciario"


def dias_da_semana(data_inicio, data_final, url, tribunal, proibido=set([5, 6])):
    dias = []
    dia = timedelta(days=1)
    if data_inicio and data_final:
        if data_final > data_inicio:
            while data_inicio <= data_final:
                if data_inicio.weekday() not in proibido:
                    dias.append(url.format(tribunal, "%02d" % data_inicio.year,
                                           "%02d" % data_inicio.month,
                                           "%02d" % data_inicio.day))
                data_inicio = data_inicio + dia
            return dias
        else:
            print "Data Final menor do que a inicial"
            return False
    else:
        return


def data(data_busca=datetime.now(), url=url_base, proibido=set([5, 6])):
    dias = []
    tribunais = range(1, 25)
    nao_possui = [6]
    dia = timedelta(days=1)
    d = data_busca - dia
    if d.weekday() not in proibido:
        for tribunal in tribunais:
            if tribunal not in nao_possui:
                dias.append({'url': url.format(tribunal, "%02d" % d.year,
                                               "%02d" % d.month,
                                               "%02d" % d.day),
                             'tribunal': 'TRT' + str(tribunal)})
        return dias
    else:
        return False


def acessar_url(url):
    # Cabecalho
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0'
    }
    sessao = requesocks.Session()
    sessao.proxies = {'http': 'socks5://127.0.0.1:9050'}
    try:
        acesso = sessao.get(url, headers=headers)
        if acesso.status_code == 200:
            return acesso
    except Exception as e:
        print e
        return False


def extrair_conteudo(conteudo):
    return re.findall(r'\d{7}\-\d+\.\d{4}\.\d\.\d{2}\.\d+', conteudo, re.UNICODE)


def paginacao(conteudo, tribunal):
    proxima_pagina = True
    count = 1
    erros = 0
    lista_processo = set()
    while proxima_pagina and erros < 4:
        proxima_pagina = conteudo.url + '?p=%d' % count
        acesso = acessar_url(proxima_pagina)
        dados = extrair_conteudo(acesso.content)
        if dados:
            for dado in dados:
                lista_processo.add(dado)
            count += 1
        else:
            erros += 1
    if lista_processo:
        for processo in lista_processo:
            print processo, tribunal
            try:
                Processos.objects.create(numero=processo, tribunal=tribunal)
            except:
                pass
    return True


if __name__ == '__main__':
    from tasks import datas_trt_jus
    dados = data(datetime.now())
    for dado in dados:
        print dado
        datas_trt_jus.delay(dado)