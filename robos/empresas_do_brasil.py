#!/usr/bin/env python
# encoding: utf-8

__author__ = 'joaotrevizoliesteves'


import requesocks
import sys
import re
from lxml import html
from time import sleep
from testes.cidades import *
from datetime import datetime
from models import *
from utilitarios import fix_bad_unicode
from TorCtl import TorCtl
from random import choice


connect('processos')

headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0',
            'Content-Type': 'text/html; application/x-www-form-urlencoded; charset=UTF-8'
    }

tipos_empresas = [
    u'acougues',
    u'advogados',
    u'agencias-de-viagens',
    u'bancos',
    u'bufes',
    u'cabeleireiros',
    u'caixas-eletronicos',
    u'cartorios',
    u'chaveiros',
    u'comercio-alimenticio-e-diversificados',
    u'comercio-de-material-de-construcao',
    u'contabilidades',
    u'estetica-e-beleza',
    u'farmacias',
    u'floriculturas',
    u'hoteis',
    u'imobiliarias',
    u'informatica',
    u'joalherias-e-relojoarias',
    u'lan-houses',
    u'lavanderias',
    u'livrarias-papelarias-e-bancas-de-jornais',
    u'locacao-de-automoveis',
    u'lojas-de-artigos-esportivos',
    u'lojas-de-brinquedos-e-artigos-recreativos',
    u'lojas-de-eletrodomesticos-audio-e-video',
    u'lojas-de-moveis-e-colchoes',
    u'lojas-de-roupas-calcados-e-acessorios',
    u'lotericas',
    u'oticas',
    u'outros-servicos-de-hospedagem',
    u'padarias',
    u'pet-shops',
    u'restaurantes-bares-e-lanchonetes',
    u'servicos-alimenticios',
    u'servicos-financeiros',
    u'supermercados-e-similares',
    u'veterinarios',
    u'clinicas-e-consultorios',
    u'exames',
    u'hospitais',
    u'pronto-socorros',
    u'servicos-adicionais',
    u'servicos-de-apoio',
    u'unidades-de-atendimento',
    u'auto-escolas',
    u'ensino-de-concursos',
    u'escolas-de-danca',
    u'estabelecimentos-de-educacao-infantil',
    u'estabelecimentos-de-educacao-profissional',
    u'estabelecimentos-de-educacao-superior',
    u'colegios-de-ensino-fundamental-e-medio',
    u'escolas-de-idiomas',
    u'outras-atividades-de-ensino',
    u'servicos-de-apoio-a-educacao',
    u'alinhamento-e-balanceamento',
    u'auto-eletricas',
    u'borracharias',
    u'concessionarias-de-automoveis-motos-e-pecas',
    u'funilarias-e-pintura-de-automoveis',
    u'lojas-de-lubrificantes',
    u'manutencao-de-automoveis-e-motos',
    u'mecanicas-de-automoveis',
    u'mecanicas-de-motos',
    u'postos-de-gasolina',
    u'aluguel-de-equipamentos',
    u'arquitetura-e-engenharia',
    u'captacao-tratamento-e-distribuicao-de-agua',
    u'coleta-e-tratamento-de-residuos',
    u'construcao-de-edificios',
    u'construcao-em-geral',
    u'esgoto-e-atividades-relacionadas',
    u'fornecimento-de-eletricidade-e-gas',
    u'obras-de-infra-estrutura',
    u'armazenamento',
    u'organizacao-do-transporte-de-carga',
    u'servicos-de-entrega',
    u'transporte-aereo',
    u'transporte-maritimo',
    u'transporte-terrestre',
    u'horticultura-e-floricultura',
    u'lavoura-permanente',
    u'lavoura-temporaria',
    u'mudas-e-sementes-certificadas',
    u'pecuaria',
    u'pesca-e-aquicultura',
    u'producao-florestal',
    u'servicos-auxiliares-agricolas',
    u'artes-e-espetaculos',
    u'atividades-de-apoio',
    u'atividades-esportivas',
    u'bibliotecas',
    u'criacao-artistica',
    u'jardins-parques-e-zoologicos',
    u'jogos-de-azar-e-apostas',
    u'museus-e-predios-historicos',
    u'recreacao-e-lazer',
    u'comercio-atacadista-de-produtos-alimenticios',
    u'comercio-atacadista-agropecuario',
    u'comercio-atacadista-de-equipamentos-de-uso-domestico',
    u'comercio-atacadista-de-maquinas-e-equipamentos',
    u'comercio-atacadista-de-produtos-cosmeticos-e-similiares',
    u'comercio-atacadista-de-produtos-de-construcao-e-materia-prima',
    u'comercio-atacadista-de-produtos-de-escritorio-livros-jornais-e-outros',
    u'comercio-atacadista-de-produtos-de-ti-e-comunicacao',
    u'comercio-atacadista-de-produtos-farmaceuticos',
    u'comercio-atacadista-de-produtos-medicos-e-similares',
    u'comercio-atacadista-de-tecidos-vestuario-e-similares',
    u'comercio-atacadista-em-geral',
    u'comercio-de-automoveis-motos-e-pecas',
    u'comercio-de-produtos-de-uso-domestico',
    u'agencias-matrimoniais',
    u'atividades-funerarias',
    u'atividades-juridicas',
    u'cuidados-com-animais',
    u'desenvolvimento-de-software-ti-e-servicos-de-informacao',
    u'design-e-decoracao-de-interiores',
    u'fotografia-e-filmagem',
    u'gestao-de-ativos',
    u'leiloes',
    u'locacao-de-meios-de-transporte',
    u'locacao-de-produtos-diversos',
    u'manutencao-de-produtos-domesticos',
    u'outras-atividades',
    u'outros-servicos',
    u'outros-servicos-imobiliarios',
    u'paisagismo',
    u'pesquisa-e-desenvolvimento-cientifico',
    u'producao-cinematografica-som-televisao-e-similares',
    u'publicidade-e-pesquisa-de-mercado',
    u'representantes-comerciais',
    u'seguro-previdencia-e-planos-de-saude',
    u'selecao-e-agenciamento-e-locacao-de-mao-de-obra',
    u'servicos-adicionais-de-arquitetura-e-engenharia',
    u'servicos-de-apoio-a-edificios',
    u'servicos-de-apoio-administrativo-para-empresas',
    u'servicos-de-edicao-e-impressao',
    u'servicos-de-limpeza',
    u'telecomunicacoes',
    u'traducao-e-interpretacao',
    u'vigilancia-e-seguranca',
    u'orgaos-da-administracao-publica',
    u'organismos-internacionais-e-outras-instituicoes-extraterritoriais',
    u'organizacoes-religiosas',
    u'organizacoes-sindicais',
    u'outras-organizacoes',
    u'confeccao-de-artigos-do-vestuario',
    u'extracao-de-carvao-petroleo-e-gas-natual',
    u'extracao-de-minerais-metalicos',
    u'extracao-de-minerais-nao-metalicos',
    u'fabricacao-de-automoveis-reboques-e-carrocerias',
    u'fabricacao-de-bebidas',
    u'fabricacao-de-celulose-papel-e-produtos-de-papel',
    u'fabricacao-de-derivados-do-petroleo-e-biocombustiveis',
    u'fabricacao-de-embarcacoes-aeronaves-militares-e-outros',
    u'fabricacao-de-farmoquimicos-e-farmaceuticos',
    u'fabricacao-de-maquinas-e-equipamentos',
    u'fabricacao-de-maquinas-aparelhos-e-materiais-eletricos',
    u'fabricacao-de-moveis',
    u'fabricacao-de-produtos-alimenticios',
    u'fabricacao-de-produtos-de-borracha-e-plastico',
    u'fabricacao-de-produtos-de-informatica-e-eletronicos',
    u'fabricacao-de-produtos-de-madeira',
    u'fabricacao-de-produtos-de-metal',
    u'fabricacao-de-produtos-diversos',
    u'fabricacao-de-produtos-do-fumo',
    u'fabricacao-de-produtos-minerais-nao-metalicos',
    u'fabricacao-de-produtos-quimicos',
    u'fabricacao-de-produtos-texteis',
    u'impressao-e-reproducao-de-gravacoes',
    u'manutencao-e-instalacao-de-maquinas-e-equipamentos',
    u'metalurgia',
    u'preparacao-e-fabricacao-de-produtos-de-couro',
]
estados = [u'rj', u'sp', u'mg', u'rs',
           u'ba', u'pe', u'ce', u'pa',
           u'ap', u'pr', u'df', u'to',
           u'am', u'rr', u'sc', u'pb',
           u'ro', u'ac', u'ma', u'es',
           u'go', u'al', u'se', u'rn',
           u'pi', u'mt', u'ms']

def limpando_latin_utf(string):
    return fix_bad_unicode(string.encode('UTF-8').decode('latin-1')).lower().strip()


def newId():
    '''
    tor
    '''
    conn = TorCtl.connect(controlAddr="127.0.0.1", controlPort=9051, passphrase="D1g1n3tT@")
    conn.send_signal("NEWNYM")
    conn.close()

def newProxie():
    with open('proxist.txt', 'r') as f:
        lista = f.read()
        nova_lista = lista.split('\n')[:-1]
    proxie_morto = True
    while proxie_morto:
        novo_proxie = choice(nova_lista)
        sessao = requesocks.session()
        sessao.proxies = {'http': novo_proxie,
                          'https': novo_proxie}
        try:
            print novo_proxie
            sec0 = sessao.get('http://www.reddit.com', headers=headers, timeout=4)
            if sec0.status_code == 200:
                proxie_morto = False
                print "conectado no proxy"
                print sec0.status_code
                return novo_proxie
            else:
                print "codigo errado", sec0.status_code
                pass
        except Exception as e:
            print 'erro de proxie: ', e
            sleep(4)
            pass

sessao = requesocks.session()
proxie = newProxie()
sessao.proxies = {'http': proxie,
                  'https': proxie}


def acessar_url(url, method=('GET',)):
    """
    Funcao para acessar as urls
    :param url: str
    :return: object response
    """
    sessao_belza = True

    # sessao.proxies = {'http': '127.0.0.1:8118'}
    # sessao.proxies = {'http': '119.6.144.74:83'}

    while sessao_belza:
        sleep(4)
        contador = 0
        if 'GET' in method:
            try:
                try:
                    conteudo = sessao.get(url, headers=headers, timeout=10)
                    if conteudo.status_code == 200:
                        # sleep(5)
                        # print(conteudo.content)
                        bloquiado = html.fromstring(conteudo.content).xpath(u'//span[contains(text(), "Seu acesso foi bloqueado")]')
                        print bloquiado
                        if len(bloquiado) == 0:
                            sessao_belza = False
                            # print conteudo.content
                            # print html.fromstring(conteudo.content).xpath('//div[@id = "div_ip"]/text()')[0]
                            return conteudo
                        else:
                            print 'gerando novo ip'
                            proxie = newProxie()
                            sessao.proxies = {'http': proxie,
                                              'https': proxie}
                            contador += 1
                            sleep(10)
                        if contador > 5:
                            sessao_belza = False
                            return False
                    else:
                        proxie = newProxie()
                        sessao.proxies = {'http': proxie,
                                      'https': proxie}
                except:
                    proxie = newProxie()
                    sessao.proxies = {'http': proxie,
                                      'https': proxie}
            except Exception as e:
                print e, sys.exc_info()[2].tb_lineno
                pass


def paginacao(link, estado):
    tem_pagina = True
    conteudo = acessar_url(link)
    try:
        while tem_pagina:
            # sleep(4)
            if conteudo.status_code == 200:
                bloquiado = html.fromstring(conteudo.content).xpath(u'//span[contains(text(), "Seu acesso foi bloqueado")]')
                if len(bloquiado) == 0:
                    root = html.fromstring(conteudo.content)
                    links_empresas = root.xpath('//td/a[contains(@href, "empresa")]/@href')
                    lista_empresas = ['http://empresasdobrasil.com%s' % url_href for url_href in links_empresas]
                    print lista_empresas
                    for empresa_pagina in lista_empresas:
                        print empresa_pagina
                        try:
                           ja_foi = Empresas_cadastradas.objects.get(url=empresa_pagina)
                        except:
                           ja_foi = False
                        if ja_foi == False:
                            sessao_beleza = True
                            while sessao_beleza:
                                conteudo_empresa = acessar_url(empresa_pagina)
                                bloq = html.fromstring(conteudo_empresa.content).xpath(u'//span[contains(text(), "Seu acesso foi bloqueado")]')
                                if len(bloq) == 0:
                                    sessao_beleza = False
                                    if conteudo_empresa.status_code == 200:
                                        root_empresa = html.fromstring(conteudo_empresa.content)
                                        try:
                                            razao_social_pre = root_empresa.xpath(u'//h4/strong[contains(text(), "Razão Social")]/../following-sibling::h5/text()')[0]
                                            razao_social = limpando_latin_utf(razao_social_pre)
                                            print razao_social
                                        except Exception as e:
                                            razao_social =None
                                            print e
                                            pass
                                        try:
                                            nome_fantasia_pre = root_empresa.xpath(u'//h4/strong[contains(text(), "Nome fantasia")]/../following-sibling::h5/text()')[0]
                                            nome_fantasia = limpando_latin_utf(nome_fantasia_pre)
                                        except:
                                            nome_fantasia = None
                                        try:
                                            cnpj_pre = root_empresa.xpath(u'//h4/strong[contains(text(), "CNPJ")]/../following-sibling::h5/text()')[0]
                                            # cnpj = unicode(cnpj_pre.replace('.', '').replace('/', '').replace('-', ''))
                                            cnpj = re.sub(u'[./-]', u'', cnpj_pre)
                                        except:
                                            cnpj = None
                                        try:
                                            data_abertura_pre = root_empresa.xpath(u'//h4/strong[contains(text(), "Data da abertura")]/../following-sibling::h5/text()')[0]
                                            data_abertura = datetime.datetime.strptime(data_abertura_pre, '%d/%m/%Y')
                                        except:
                                            data_abertura = None
                                        try:
                                            status_empresa_pre = root_empresa.xpath(u'//h4/strong[contains(text(), "Status da empresa")]/../following-sibling::h5/text()')[0]
                                            status_empresa = limpando_latin_utf(status_empresa_pre)
                                        except:
                                            status_empresa = None
                                        try:
                                            natureza_juridica_pre = limpando_latin_utf(root_empresa.xpath(u'//h4/strong[contains(text(), "Natureza jurídica")]/../following-sibling::h5/text()')[0])
                                            cod_nat =  natureza_juridica_pre.split(u' - ')
                                            natureza_juridica = cod_nat[1]
                                            cod_natureza_juridica = cod_nat[0]

                                        except:
                                            natureza_juridica = None
                                            cod_natureza_juridica = None
                                        try:
                                            rua_pre = root_empresa.xpath(u'//h4/strong[contains(text(), "Endereço")]/../following-sibling::h5/text()')
                                            print rua_pre
                                            rua = limpando_latin_utf(rua_pre[0]).split(u', ')[0]
                                            numero_rua = (limpando_latin_utf(rua_pre[0]).split(u', ')[1]).replace(u'.', u'')
                                            bairro = limpando_latin_utf(rua_pre[1]).replace(u'bairro ', u'')
                                            cidade_pre =root_empresa.xpath(u'//h4/strong[contains(text(), "Endereço")]/../following-sibling::div/h5/text()')
                                            cidade = limpando_latin_utf(cidade_pre[0]).replace(u'cidade ', u'')
                                            cep = limpando_latin_utf(re.sub(u'[.-]', u'', rua_pre[2])).replace(u'cep ', u'')
                                            telefone_pre = limpando_latin_utf(rua_pre[3]).replace(u'telefone: ', u'')
                                            if telefone_pre != u'não disponível':
                                                telefone = telefone_pre
                                            else:
                                                telefone = None
                                            print telefone, telefone_pre
                                            print cidade
                                            print cep
                                            print bairro
                                            print rua
                                            print(numero_rua)
                                        except:
                                            rua = None
                                            numero_rua = None
                                            bairro = None
                                            cidade = None
                                            cep = None
                                            telefone = None
                                        try:
                                            atividades_prin = limpando_latin_utf(root_empresa.xpath(u'//h4/strong[contains(text(), "Atividade econômica principal")]/../following-sibling::div/a/text()')[0])
                                            atividades_sec_pre = root_empresa.xpath(u'//h4/strong[contains(text(), "Atividades econômicas secundárias")]/../following-sibling::div/div/a/text()')
                                            atividades_sec = [limpando_latin_utf(atv) for atv in atividades_sec_pre]
                                            if len(atividades_sec) == 0:
                                                atividades_sec =None
                                            print atividades_prin
                                            print atividades_sec
                                        except:
                                            atividades_prin = None
                                            atividades_sec = None
                                        print natureza_juridica
                                        try:
                                            dado_url = Empresas_cadastradas(url=empresa_pagina)
                                            dado_url.save()
                                            dado = Empresas(cnpj=cnpj,
                                                            razao=razao_social,
                                                            nome_fantasia=nome_fantasia,
                                                            data_abertura=data_abertura,
                                                            status=status_empresa,
                                                            natureza_juridica=natureza_juridica,
                                                            cod_natureza_juridica=cod_natureza_juridica,
                                                            rua=rua,
                                                            numero_rua=numero_rua,
                                                            bairro=bairro,
                                                            cidade=cidade,
                                                            estado=estado,
                                                            cep=cep,
                                                            telefone=telefone,
                                                            atividade_primaria=atividades_prin,
                                                            atividade_secundaria=atividades_sec,
                                                            processos=[],
                                                            )
                                            dado.save()


                                        except Exception as e:
                                            print 'Erro : %s, linha: %s' % (e,  sys.exc_info()[2].tb_lineno)
                                else:
                                    pass
                        else:
                            try:
                                proxima = root.xpath(u'//a[contains(text(), "»")]/@href')[0]
                                print(proxima)
                                if proxima != u'javascript:void(0);':
                                    conteudo = acessar_url('http://empresasdobrasil.com%s' % proxima)
                                    pass
                                else:
                                    tem_pagina = False
                                    pass
                            except Exception as e:
                                print e , sys.exc_info()[2].tb_lineno
                                tem_pagina = False
                    try:
                        proxima = root.xpath(u'//a[contains(text(), "»")]/@href')[0]
                        print(proxima)
                        if proxima != u'javascript:void(0);':
                            conteudo = acessar_url('http://empresasdobrasil.com%s' % proxima)
                        else:
                            tem_pagina = False
                    except Exception as e:
                        print e , sys.exc_info()[2].tb_lineno
                        tem_pagina = False
            else:
                proxie = newProxie()
                sessao.proxies = {'http': proxie,
                                  'https': proxie}
                pass
    except Exception as e:
        print e, sys.exc_info()[2].tb_lineno


def gerar_links_tipos(sigla, cidade):
    """
    Funcao que extrai os links de cada tipo de empresa.

    """
    urls = []
    url_pre = u"http://empresasdobrasil.com/empresas/{cidade}-{estado}/{tipo}/filtro-bairro"
    # for cidade in estado:
    #     cidade_format = re.sub(u'[ ]', u'-', cidade)
    #     for tipo in tipos_empresas:
    #         url_pronta = url_pre.format(cidade=cidade_format, estado=sigla, tipo=tipo)
    #         urls.append(unicode(url_pronta))
    cidade_format = re.sub(u'[ ]', u'-', cidade)
    for tipo in tipos_empresas:
        url_pronta = url_pre.format(cidade=cidade_format, estado=sigla, tipo=tipo)
        urls.append(unicode(url_pronta))
    return urls

def extrair_links_tipos(lista_link, estado):
    """
    Funcao que extrai os links de cada tipo de empresa.
    """
    for link in lista_link:
        url = link
        print url
        conteudo = acessar_url(url)
        sessao_belza = True
        while sessao_belza:
            if conteudo.status_code == 200:
                bloquiado = html.fromstring(conteudo.content).xpath(u'//span[contains(text(), "Seu acesso foi bloqueado")]')
                print bloquiado
                if len(bloquiado) == 0:
                    sessao_belza = False

                    root = html.fromstring(conteudo.content)
                    try:

                        bairros = root.xpath(u'//a[contains(@href, "bairro")]/@href')
                        print bairros
                        if len(bairros) != 0:
                            lista_bairro = [ u'http://empresasdobrasil.com%s' % url_bairro for url_bairro in bairros]
                            for bairro in lista_bairro:
                                # sleep(4)
                                paginacao(bairro, estado)
                        else:
                            print url
                            bloquiado = html.fromstring(conteudo.content).xpath(u'//span[contains(text(), "bloqueado")]')
                            print bloquiado
                            if len(bloquiado) == 0:
                                pass
                            else:
                                sessao = requesocks.session()
                                proxie = newProxie()
                                sessao.proxies = {'http': proxie,
                                                  'https': proxie}
                            print "Pagina Vazia"
                    except Exception as e:
                        print "Erro ao extrair os links dos bairros: %s, linha %s" % (e, sys.exc_info()[2].tb_lineno)
                        pass
                else:
                    sessao = requesocks.session()
                    proxie = newProxie()
                    sessao.proxies = {'http': proxie,
                                      'https': proxie}
            else:
                pass
            # sleep(3)


# print extrair_links_tipos('sao-paulo', 'sp', 'padarias')
# lista_link = gerar_links_tipos(sp, u'sp')
lista_link = gerar_links_tipos(u'sp', u'ribeirao preto')
print len(lista_link)
print extrair_links_tipos(lista_link, u'sp')
# paginacao('http://empresasdobrasil.com/empresas/jaboticabal-sp/bairro-centro/fabricacao-de-moveis')
# paginacao('http://empresasdobrasil.com/empresas/sao-paulo-sp/bairro-aclimacao/padarias')
# for tASDASD in range(1, 10, 1):
#     print(acessar_url("http://www.meuip.com.br/"))
#     sleep(1)
