#!/usr/bin/env python
# encoding: utf-8
__author__ = 'joaotrevizoliesteves'
import re
import requests
from robos.models import *
from lxml import html

url_processo = 'http://tjdf19.tjdft.jus.br/cgi-bin/tjcgi1?NXTPGM=tjhtml101&submit=ok&SELECAO=3&' \
               'CHAVE=DF12250&CIRC=ZZ&CHAVE1=&ORIGEM=INTER'


# advogados = re.compile(r'[A-Z]{2}\d{3,}\w')
advogados = re.compile((r'([A-Z]{2}\d+(?:\s+\w+[A-Z])+)|([A-Z]{2}\d+[A-Z](?:\s+\w+[A-Z])+)'), re.UNICODE)

def limpeza(palavra):
    '''
    recebe palavra ou lista de palavras
    retorna frases limpas ou lista de palavras limpas
    '''
    if type(palavra) == list:
        lista_limpa=[]
        for palavra_lista_limpa in palavra:
            lista_limpa.append(re.sub(r'\s+', ' ', palavra_lista_limpa).strip())
        return lista_limpa

    elif palavra:
        return re.sub(r'\s+', ' ', palavra).strip()


    else:
        print u"Não temos a palavra"
        return False


def coletar_advogados(conteudo):
    advs = set()
    adv_encontrados = re.findall(advogados, conteudo)
    for setar in adv_encontrados:
        for seted in setar:
            if seted !='':
                advs.add(seted)
    # return advs,


def coleta_dados(url): #recebe a url dos processos do advogado, sera modificada em breve
    '''
    url = str()
    '''
    processos = [] #lista de processos deste advogado a ser retornada
    try:
        site = requests.get(url) #tenta pegar essa url
    except Exception as e:
        site = None
        print "Erro: ", e

    if site.status_code == 200: #verifica o status
        conteudo = site.content #conteudo é o conteudo do site

    else:
        print "Erro!: %s" % site.status_code #caso contrario
        conteudo = False # falso

    if conteudo:
        miolo = html.fromstring(conteudo)
        for unorded_lists in enumerate(miolo.xpath('//font/ul')):
            links_processos = miolo.xpath('//font/ul[%s]/table/tr' % (unorded_lists[0] + 1))
            for link in enumerate(links_processos):
                processos = link[1].xpath('//li/a/@href')

    if processos > 0:
        return processos
    else:
        print u'Não Foram encontrados processos para este advogado meu jovem'

def pegar_os_dados(url):
    '''
    url = str(url do processo)
    '''
    resultados = {}
    erro_conexao = 0
    try:
        site = requests.get(url)
    except Exception as e:
        site = None
        print "Erro: ", e

    if site.status_code == 200:
        conteudo = site.content

    elif site.status_code == 403:
        return

    else:
        print "Erro!: %s" % site.status_code
        conteudo = False

    if conteudo:
        miolo = html.fromstring(conteudo)
        print(coletar_advogados(conteudo))
        processo  = miolo.xpath('//span[@id="i_numeroProcesso20"]/text()')[0]
        trinunal  = 'TJDFT'
        juiz = ""
        local_fisico = ""

        try:
            area = limpeza(miolo.xpath('//span[@id="i_materiaProcesso"]/text()')[0])
        except:
            area = ""
        try:
            assunto = limpeza(miolo.xpath('//span[@id="i_assuntoProcessual"]/text()')[0])
            print assunto.find()
        except:
            assunto = ""
        try:
            classe = limpeza(miolo.xpath('//span[@id="i_classeProcessual"]/text()')[0])
        except:
            classe = ""
        try:
            distribuicao = limpeza(miolo.xpath('//span[@id="i_dataDistribuicao"]/text()')[0])
        except:
            distribuicao = ""
        try:
            v = limpeza(miolo.xpath('//span[@id="i_valorCausa"]/text()')[0])
            valor = u'R$ %s' % v
        except:
            valor = ""
        try:
            vara = limpeza(miolo.xpath('//span[@id="i_enderecoVara"]/text()')[0])
            cidade = limpeza(miolo.xpath('//span[@id="i_nomeCircunscricao"]/text()')[0])
            origem = vara + " " + cidade

        except:
            origem = ""

        try:
            data_movimentacoes = limpeza(miolo.xpath('//table/tr[position()>3]/td[1]/font/b/span/text()'))
            lista_movimentacoes = limpeza(miolo.xpath('//table/tr[position()>3]/td[2]/font/span/text()'))
            movimentacao = zip(data_movimentacoes, lista_movimentacoes)

        except:
            movimentacoes =""


        partes_formatada = []
        try:
            url_parte =miolo.xpath('//a[contains(text(), "Advogados das Partes")]/@href')[0]

            try:
                site_partes = requests.get(url_parte)
                conteudo_partes = site_partes.content
                miolo = html.fromstring(conteudo_partes)
                titulos = miolo.xpath('//body/b')
                partes = []
                for titulo in enumerate(titulos):
                    parte = []
                    if titulo[0] > 2:
                        b_titulo = ('//body/b[%s]' % (titulo[0] + 1) )
                        titulo = miolo.xpath((b_titulo + '/text()'))[0]
                        pessoa = miolo.xpath((b_titulo + '/b/text()'))[0]
                        parte.append(titulo)
                        parte.append(pessoa)
                        partes.append(parte)
                titulos = miolo.xpath('//body/b[4]/following-sibling::text()')

                for iterador in partes:
                    nova_parte = iterador
                    while True:
                        if (titulos[0] + titulos[1]) != '\n\n':
                            nova_parte.append(titulos[0])
                            del titulos[0]
                        else:
                            del titulos[0:2]
                            break
                    dict_append = {}
                    dict_append['parte'] = limpeza(nova_parte[0]).lower()
                    dict_append['pessoa'] = limpeza(nova_parte[1]).lower()
                    dict_append['advogado'] = (limpeza(", ".join(nova_parte[2:]))).replace('Advogado : ', '').lower()
                    partes_formatada.append(dict_append)

            except Exception as e:
                print "Erro: %s" % e

        except Exception as e:
            print e
            print u'não foi encontrado o link'

        resultados['url'] = url
        resultados['processo'] = processo
        resultados['tribunal'] = trinunal
        resultados['juiz'] = juiz
        resultados['local_fisico'] = local_fisico
        resultados['area'] = area
        resultados['assunto'] = assunto
        resultados['classe'] = classe
        resultados['distribuicao'] = distribuicao
        resultados['valor'] = valor
        resultados['orgao_origem'] = origem
        resultados['movimentacoes'] = movimentacao
        resultados['partes'] = partes_formatada
        return resultados

# print pegar_os_dados("http://tjdf19.tjdft.jus.br/cgi-bin/tjcgi1?NXTPGM=tjhtml105&ORIGEM=INTER&SELECAO=1&CIRCUN=1&CDNUPROC=20040110379567")

for x in coleta_dados(url_processo):
    print(pegar_os_dados(x))
# print pegar_os_dados('http://tjdf19.tjdft.jus.br/cgi-bin/tjcgi1?NXTPGM=tjhtml105&ORIGEM=INTER&SELECAO=1&CIRCUN=1&CDNUPROC=20000110230877')
# print(coleta_dados(url_processo))

# miolo.xpath('//span[@id=""]/text()')[0]