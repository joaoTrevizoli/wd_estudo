#!/usr/bin/env python
# encoding: utf-8
__author__ = 'joaotrevizoliesteves'

import re
import argparse
import datetime
import requesocks
from robos.padronizador import atualizar_banco
from lxml.html.clean import Cleaner
from lxml import html
from robos.models import Processos, Partes, Movimentacoes
from robos.utilitarios import fix_bad_unicode

url_base = 'http://pje.{tri}.jus.br/consultaprocessual/pages/consultas/ListaProcessos.seam?numero_unic={num}&cid=12223'

def acessar_url(url, redirecionar=False):
    # Cabecalho
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0'
    }
    sessao = requesocks.session()
    sessao.proxies = {'http': 'socks5://127.0.0.1:9050'}
    try:
        if redirecionar:
            acesso = sessao.get(url, headers=headers, verify=False, allow_redirects=True)
        else:
            acesso = sessao.get(url, headers=headers, verify=False)
        if acesso.status_code == 200:
            #print acesso.content
            #print acesso.url
            return acesso
    except Exception as e:
        print "Erro: %s" % e
        return


def limpeza(palavra):
    """
    Recebe palavra ou lista de palavras,
    retorna frases limpas ou lista de palavras limpas
    """
    if type(palavra) == list:
        lista_limpa = []
        for palavra_lista_limpa in palavra:
            limpo = re.sub(r'\s+', ' ', palavra_lista_limpa).strip()
            if limpo != '':
                lista_limpa.append(limpo)
        return lista_limpa

    elif palavra:
        return re.sub(r'\s+', ' ', palavra).strip()

    else:
        return False


def extrair_numprocesso(conteudo):
    u"""
    Função que recebe o conteudo e extrai o numero do processo
    """
    return re.findall(r'.*(\d).*\s(\w+)\-(\d{7}\-\d+\.\d{4}\.\d\.\d{2}\.\d+)', conteudo, re.UNICODE)


def extrair_conteudo(conteudo, tribunal):
    if conteudo:
        cleaner = Cleaner(javascript=True)  # Remove os js da pagina
        texto_limpo = cleaner.clean_html(conteudo.content)
        root_limpo = html.fromstring(texto_limpo)  # html sem nenhum js
        root_nao_limpo = html.fromstring(conteudo.content)  # html com os js(as vezes é necessario)

        resultado = {}
        try:
            primeiralinha = extrair_numprocesso(root_limpo.xpath(re.sub(r'\s', '', ('//div'
                                                                                    '[@id = "panelDetalhesProcesso_header"]'
                                                                                    '/text()')))[0])[
                0]  # num_processo e instância - precisa limpar e separar
            processo = primeiralinha[2]
            if not processo:
                return False
            instancia = primeiralinha[0]
            classe = primeiralinha[1]
            juiz = None
            valor = None
            tribunal = tribunal
            url = conteudo.url
            try:
                origem = root_limpo.xpath('.//*[@id="panelDetalhesProcesso_header"]/font/text()')[0]  # Origem
            except:
                origem = None
            try:
                assunto = ', '.join(limpeza(root_nao_limpo.xpath('//select[@id = "assuntosDecorate:assuntosSelect"]'
                                                                 '//option/text()')))  # assunto falta limpar e separar
            except:
                assunto = None
            try:
                tipos_txt = limpeza(root_nao_limpo.xpath(
                    '//span[@id = "j_id68"]/div/div/dl/dd[@class = "t11"]/'  # procura por tipos de partes
                    'preceding-sibling::dt/text()'))
            except:
                tipos_txt = ['Autor', u'Réu']  # Caso não encontre utilizar partes padroes
            try:
                lista_partes = []  # recebe as partes categorizadas
                dict_autor = {}
                dict_reu = {}
                # Checa se os autores possuem a id panelDetalhes
                if len(root_limpo.xpath('//*[@id="partesReclamanteFieldDecorate:panelDetalhes"]//'
                                        'img[contains(@src,"parte.png")]/../following-sibling::td/text()')):

                    nomes_autores = root_limpo.xpath('//*[@id="partesReclamanteFieldDecorate:panelDetalhes"]//'
                                                     'img[contains(@src,"parte.png")]/../following-sibling::td/text()')
                    for adv_autor in nomes_autores:  #constroi o dict da parte para inserir
                        # print adv_autor
                        dict_autor['nome'] = unicode(adv_autor.lower())
                        dict_autor['tipo'] = unicode((tipos_txt[0].lower()).replace('(s):', ''))
                        advs_autores = root_limpo.xpath(u'//*[@id="partesReclamanteFieldDecorate:panelDetalhes"]//'
                                                        u'img[contains(@src,"representante.png")]/..'
                                                        u'/following-sibling::td[contains(@id, "%s")]/text()' % unicode(adv_autor))
                        advs_autores = [fix_bad_unicode(unicode(adv))for adv in advs_autores]
                        dict_autor['advogados'] = unicode(((', '.join(advs_autores)).replace('ADVOGADO: ', '')).lower())
                        lista_partes.append(dict_autor.copy())

                #Caso contrario usa a id panelMais
                else:
                    nomes_autores = root_limpo.xpath('//*[@id="partesReclamanteFieldDecorate:panelMais"]//'
                                                     'img[contains(@src,"parte.png")]/../following-sibling::td/text()')
                    for adv_autor in nomes_autores:  #constroi o dict da parte para inserir
                        dict_autor['nome'] = unicode(adv_autor.lower())
                        dict_autor['tipo'] = unicode((tipos_txt[0].lower()).replace('(s):', ''))
                        advs_autores = root_limpo.xpath(u'//*[@id="partesReclamanteFieldDecorate:panelMais"]//'
                                                        u'img[contains(@src,"representante.png")]/../'
                                                        u'following-sibling::td[contains(@id, "%s")]/text()' % unicode(adv_autor))
                        advs_autores = [fix_bad_unicode(unicode(adv))for adv in advs_autores]
                        dict_autor['advogados'] = unicode(((', '.join(advs_autores)).replace('ADVOGADO: ', '')).lower())
                        lista_partes.append(dict_autor.copy())

                #Agora checa os Reus para a id panelDetalhes
                if len(root_limpo.xpath('//*[@id="partesReclamadaFieldDecorate:panelDetalhes"]//'
                                        'img[contains(@src,"parte.png")]/../following-sibling::td/text()')):
                    nomes_reu = root_limpo.xpath('//*[@id="partesReclamadaFieldDecorate:panelDetalhes"]//'
                                                 'img[contains(@src,"parte.png")]/../following-sibling::td/text()')
                    for adv_reu in nomes_reu:  #constroi o dict da parte para inserir
                        dict_reu['nome'] = unicode(adv_reu.lower())
                        dict_reu['tipo'] = (tipos_txt[1].lower()).replace('(s):', '')
                        advs_reu = root_limpo.xpath(u'//*[@id="partesReclamadaFieldDecorate:panelDetalhes"]//'
                                                    u'img[contains(@src,"representante.png")]/../'
                                                    u'following-sibling::td[contains(@id, "%s")]/text()' % adv_reu)
                        advs_reu = [fix_bad_unicode(unicode(adv))for adv in advs_reu]
                        dict_reu['advogados'] = unicode(((', '.join(advs_reu)).replace('ADVOGADO: ', '')).lower())
                        lista_partes.append(dict_reu.copy())
                else:
                    nomes_reu = root_limpo.xpath('//*[@id="partesReclamadaFieldDecorate:panelMais"]//'
                                                 'img[contains(@src,"parte.png")]/../following-sibling::td/text()')
                    for adv_reu in nomes_reu:  #constroi o dict da parte para inserir
                        dict_reu['nome'] = unicode(adv_reu.lower())
                        dict_reu['tipo'] = (tipos_txt[1].lower()).replace('(s):', '')
                        advs_reu = root_limpo.xpath(u'//*[@id="partesReclamadaFieldDecorate:panelMais"]//'
                                                    u'img[contains(@src,"representante.png")]/../'
                                                    u'following-sibling::td[contains(@id, "%s")]/text()' % adv_reu)
                        advs_reu = [fix_bad_unicode(unicode(adv))for adv in advs_reu]
                        dict_reu['advogados'] = unicode(((', '.join(advs_reu)).replace('ADVOGADO: ', '')).lower())
                        lista_partes.append(dict_reu.copy())
            except:
                lista_partes = []
            data = limpeza(root_limpo.xpath('.//*[@id="consultaProcessos:tb"]/tr/td[1]/div//text()'))
            descricao = limpeza(root_limpo.xpath(
                './/*[@id="consultaProcessos:tb"]/tr/td[2]/div//text()'))  # Movimentacao primeira linha.
            descricao = [fix_bad_unicode(unicode(d)) for d in descricao]
            movimentacoes = zip(data, descricao)
            resultado['processo'] = processo
            resultado['instancia'] = instancia
            resultado['tribunal'] = tribunal
            resultado['url'] = url
            resultado['area'] = "Trabalhista"
            resultado['assunto'] = assunto
            resultado['classe'] = classe
            resultado['distribuicao'] = data[-1]
            resultado['juiz'] = juiz
            resultado['valor'] = valor
            resultado['origem'] = origem
            resultado['local_fisico'] = None
            if len(lista_partes):
                resultado['partes'] = [Partes(nome=parte['nome'], tipo=parte['tipo'], advogados=parte['advogados'])
                                       for parte in lista_partes]  # constroi as partes

            resultado['movimentacoes'] = [Movimentacoes(data=data, descricao=dados) for data, dados in
                                          movimentacoes]  # constroi as movimentacoes
            return resultado  # retorna esse dict pronto para inserir no banco
        except Exception as e:
            print "Erro: %s, url: %s" % (e, conteudo.url)
    else:
        return False


def _extrair_links(conteudo):
    links = []
    root = html.fromstring(conteudo.content)
    linhas = root.xpath('.//*[@id="consultaProcs:tb"]/tr')
    doc = root.xpath('.//*[@id="consultaProcessualSearchForm"]/span/text()')  # Nao foi encontrado nenhum processo.
    if not doc:
        if linhas:
            for linha in linhas:
                links.append(linha.xpath('./td[1]/a/@href')[0])
            return links
        else:
            return conteudo
    else:
        return False


def primeiro_acesso(numero_processo, tribunal):
    url = 'https://pje.{tri}.jus.br{li}'
    teste = acessar_url(url_base.format(num=numero_processo, tri=tribunal.lower()), redirecionar=True)
    dados = None
    if teste:
        if teste.status_code == 200:
            links = _extrair_links(teste)
            if not links:
                return False
            else:
                try:
                    dados = extrair_conteudo(links, tribunal)
                except:
                    for link in links:
                        dados = extrair_conteudo(
                            acessar_url(url.format(li=link, tri=tribunal)),
                            tribunal)  # Acessando somente o primeito link para testes.
                if dados:
                    # atualizar(dados)
                    atualizar_banco(dados)
                else:
                    try:
                        processo = Processos.objects.get(numero=numero_processo)
                        processo.update(set__atualizado_em=datetime.datetime.now())
                        # atualizar_banco(Processos.objects.get(numero=numero_processo))
                    except:
                        pass
        else:
            return False
    else:
        return



if __name__ == '__main__':
    print
    parser = argparse.ArgumentParser(description=u'*** Acessar Processo TRT2 ***', version='%(prog)s 1.0')
    parser.add_argument('-n', dest='numero', required=True, type=str,
                        help=u'Número do processo. exemplo: 1000388-56.2014.5.02.0601.')
    args = parser.parse_args()
    primeiro_acesso(args.numero, 'TRT2')
