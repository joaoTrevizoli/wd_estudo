# encoding: utf-8
__author__ = 'joaotrevizoliesteves'

import requests
from lxml import html
from robos.utilitarios import fix_bad_unicode
from time import sleep

# url = 'https://pje.trt2.jus.br/consultaprocessual/pages/consultas/ConsultaProcessual.seam'
headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0',
        'Content-Type': 'text/html; application/x-www-form-urlencoded; charset=UTF-8'
    }
'''
print 'def insercao_comarca(trt, numero):'
for x in range(1, 25, 1):
    url = 'http://pje.trt%s.jus.br/consultaprocessual/pages/consultas/ConsultaProcessual.seam' % str(x)
    site = requests.get(url, verify=False, headers=headers)
    site_procurar = html.fromstring(site.content)
    varas = site_procurar.xpath('//option/text()')[2:]
    print 'if trt == \'TRT%s\':' % str(x)
    print 'trt_processo = {'
    for vara in varas:
        dado = fix_bad_unicode(vara.encode('UTF-8').decode('latin-1')).replace(u'ª', u'').split('-', 1)
        print u"'%s': u'%s'," % (dado[0].strip(), dado[1].strip())
    print '}'
    print 'return trt_processo[\'numero\']'
    sleep(8)


url = 'http://www.trt10.jus.br/'
site = requests.get(url, verify=False, headers=headers)
site_procurar = html.fromstring(site.content)
varas = site_procurar.xpath('//div[@id="varas_II"]/a/text()')
for vara in varas:
        dado = fix_bad_unicode(vara.encode('UTF-8').decode('latin-1')).replace(u'ª', u'').split('-', 1)
        print u"'0%s': u'%s'," % (dado[0].strip(), dado[1].strip())


url = 'http://www.trt9.jus.br/internet_base/codigovaracon.do?evento=F9-Pesquisar'
site = requests.get(url, verify=False, headers=headers)
site_procurar = html.fromstring(site.content)
numeros = site_procurar.xpath('//table[@class="delimitador tabelaFormulario"][2]/tr/td[@class="campo"][1]/text()')
codigos = site_procurar.xpath('//table[@class="delimitador tabelaFormulario"][2]/tr/td[@class="campo"][2]/text()')
varas = zip(numeros, codigos)
for x in varas:
    print u"'0%s': u'%s'," % (x[0].strip(), fix_bad_unicode(x[1].encode('UTF-8').decode('latin-1')).replace(u'ª', u'').strip())
    # print '\'', x[0].strip(), '\': ', 'u\'', fix_bad_unicode(x[1].encode('UTF-8').decode('latin-1')).replace(u'ª', u'').strip(), '\''


url = 'http://www.trt16.jus.br/site/index.php?acao=conteudo/processo/consultaPro.php'
site = requests.get(url, verify=False, headers=headers)
site_procurar = html.fromstring(site.content)
varas = site_procurar.xpath('//select[@id="varaCNJpro"]/option/text()')[1:-1]
print varas
for vara in varas:
        dado = fix_bad_unicode(vara.encode('UTF-8').decode('latin-1')).replace(u'ª', u'').split('-', 1)
        print u"'0%s': u'%s'," % (dado[0].strip().lower(), dado[1].strip().lower())

url = 'http://www.trt18.jus.br/portal/'
site = requests.get(url, verify=False, headers=headers)
site_procurar = html.fromstring(site.content)
varas = site_procurar.xpath('//select[@id="vara"]/option/text()')[1:-1]
print varas
for vara in varas:
        dado = fix_bad_unicode(vara.encode('UTF-8').decode('latin-1')).replace(u'ª', u'').split('-', 1)
        print u"'0%s': u'%s'," % (dado[0].strip().lower(), dado[1].strip().lower())
url = 'http://www.trt19.jus.br/siteTRT19/'
site = requests.get(url, verify=False, headers=headers)
site_procurar = html.fromstring(site.content)
varas = site_procurar.xpath('//select[@id="codJuntaNovo"]/option/text()')[1:-1]
print varas
for vara in varas:
        dado = fix_bad_unicode(vara.encode('UTF-8').decode('latin-1')).replace(u'ª', u'').split('-', 1)
        print u"'0%s': u'%s'," % (dado[0].strip().lower(), dado[1].strip().lower())

url = 'http://portaldeservicos.trt7.jus.br/portalservicos/buscaProcesso/externoUnidadeOrigemPopup.jsf'
site = requests.get(url, verify=False, headers=headers)
site_procurar = html.fromstring(site.content)
varas = site_procurar.xpath('//div[@id="form1:j_id4_body"]/a/text()')[1:]
print varas
for vara in varas:
        dado = fix_bad_unicode(vara.encode('UTF-8').decode('latin-1')).replace(u'ª', u'').split(' - ')
        print u"'0%s': u'%s'," % (dado[1].strip().lower(), dado[0].strip().lower().replace('-', ' '))
url = 'http://portal.trt11.jus.br/Portal/pages/numeracaounica/layout/layoutconsultanumeracaounica.jsf'
site = requests.get(url, verify=False, headers=headers)
site_procurar = html.fromstring(site.content)
varas = site_procurar.xpath('//select[@id="form1:vara"]/option/text()')[1:]
print varas
for vara in varas:
        dado = fix_bad_unicode(vara.encode('UTF-8').decode('latin-1')).replace(u'ª', u'').split('-', 1)
        print u"'0%s': u'%s'," % (dado[0].strip().lower(), dado[1].strip().lower())
url = 'http://www2.trt8.jus.br/consultaprocesso/formulario/frset_index.aspx'
site = requests.get(url, verify=False, headers=headers)
site_procurar = html.fromstring(site.content)
numeros = site_procurar.xpath('//table[@id="ctl00_ContentPlaceHolder1_GridView1"]/tr/td[1]/text()')
codigos = site_procurar.xpath('//table[@id="ctl00_ContentPlaceHolder1_GridView1"]/tr/td[2]/text()')
varas = zip(numeros, codigos)

for x in varas:
   print u"'0%s': u'%s'," % (x[0].strip(), fix_bad_unicode(x[1].encode('UTF-8').decode('latin-1')).replace(u'ª', u'').strip().lower())
 #    print '\'', x[0].strip(), '\': ', 'u\'', fix_bad_unicode(x[1].encode('UTF-8').decode('latin-1')).replace(u'ª', u'').strip(), '\''


url = 'https://www.trt13.jus.br/portalservicos/consulta/varas.jsf'
site = requests.get(url, verify=False, headers=headers)
site_procurar = html.fromstring(site.content)
numeros = site_procurar.xpath('//table[@class="richDataTable"]/tbody/tr/td[1]/a/text()')
codigos = site_procurar.xpath('//table[@class="richDataTable"]/tbody/tr/td[2]/text()')
varas = zip(numeros, codigos)
# print codigos
for x in varas:
    print u"'%s': u'%s'," % (x[1].strip(), fix_bad_unicode(x[0].encode('UTF-8').decode('latin-1')).replace(u'ª', u'').strip().lower()) '''
