#!/usr/bin/env python
# encoding: utf-8
__author__ = 'joaotrevizoliesteves'

from scipy.stats.mstats import gmean, hmean
from robos.models import *
from fuzzywuzzy import fuzz, process
import re
from collections import Counter
from gevent import monkey; monkey.patch_socket()

connect('processos')#, max_pool_size=500)
x = Processos.objects.no_cache().limit(100000)
siglas = [u'ltda', u'm.e.', u'm.e', u'me', u'm/e', u'sa', u's.a', u's/a',
          u'empresa', u'companhia', u's.a.', u'cooperativa', u'sociedade',
          u'industria', u'comercio', u'artigos', u'acessorios', u'acessórios',
          u'comércio', u'importacao', u'importação', u'exportacao', u'exportação',
          u'condominio', u'epp', u'prefeitura', u'ss', u's/s', u's.s', u'spe', u'eireli',
          u'mei', u'fundacao', u'fundação', u'varejo', u'atacado', u'varejista', u'atacadista',
          u'ltda', u'ltda.', u'outro']


# teste = u'stella comercio de moveis e decoracoes ltda - me'
#
# print process.extractBests(teste, siglas, limit=3)
# notas = []
# for processo in x:
#     partes = processo.partes
#     for nome in partes:
#         notas.append((fuzz.token_set_ratio(nome.nome.split(u' '), siglas)))
#         # print nome.nome, fuzz.token_set_ratio(nome.nome.split(u' '), siglas)
# print gmean(notas)
# dado = hmean(notas)
# notas_altas = []
# notas_baixas = []
# for processo in x:
#     partes = processo.partes
#     for nome in partes:
#         if fuzz.token_set_ratio(nome.nome.split(u' '), siglas) > 15.8674543841:
#             notas_altas.append(nome.nome)
#         else:
#             notas_baixas.append(nome.nome)
#         # print nome.nome, fuzz.token_set_ratio(nome.nome.split(u' '), siglas)
# print "empresas: ", notas_altas
# print "pessoas: ", notas_baixas
# for empresa in notas_altas:
#     print empresa
palavras = [u'de', u'da', u'dos', u'souza', u'santos', u'aparecido' u'santos', u'silva', u'ribeiro', u'santa', u'francisco',  u'das', u'gomes', u'brasil', u'barbosa', u'cristina', u'roberto', u'oliveira', u'henrique', u'carlos', u'alves', u'antonio', u'servicos', u'pereira', u'rodrigues', u'ferreira', u'lima', u'luiz', u'maria', u'jose', u'aparecido', u'aparecida', u'em', u'machado', u'regina', u'teixeira', u'carvalho', u'fernando', u'paulo', u'do']
mais=[]
for processo in x:
    partes = processo.partes
    for nome in partes:
        string = nome.nome.replace(u'-', ' ',).replace(u'  ', u' ',)
        words = re.findall(r'[\w./]+', string, re.UNICODE)

        for s in words:
            print len(s), s
            if len(s) > 1 and s not in palavras:
                mais.append(s)
print Counter(mais)