#!/usr/bin/env python
# encoding: utf-8
from requests.packages.urllib3.exceptions import HTTPError

__author__ = 'joaotrevizoliesteves'

import re
import requests
import time
from datetime import timedelta, datetime
from lxml import html
from celery import Celery
from celery.task import task

app = Celery('tasks', broker='amqp://guest@localhost//')

url_base = "http://www.jusbrasil.com.br/diarios/documentos/TRT-{0}/{1}/{2}/{3}/Judiciario"


def dias_da_semana(data_inicio, data_final, url, tribunal, proibido=set([5, 6])):
    dias = []
    dia = timedelta(days=1)
    if data_inicio and data_final:
        if data_final > data_inicio:
            while data_inicio <= data_final:
                if data_inicio.weekday() not in proibido:
                    dias.append(url.format(tribunal, "%02d"%data_inicio.year,
                                           "%02d" % data_inicio.month,
                                           "%02d"%data_inicio.day))
                data_inicio = data_inicio + dia
            return dias
        else:
            print "Data Final menor do que a inicial"
            return False
    else:
        return


def acessar_url(url):
    # Cabecalho
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0'
    }
    sessao = requests.Session()
    try:
        acesso = sessao.get(url, headers=headers)
        if acesso.status_code == 200:
            return acesso
    except HTTPError as e:
        print e
        return False


def extrair_conteudo(conteudo):
    return re.findall(r'\d+\-\d+\.\d+\.\d\.\d+\.\d+', conteudo, re.UNICODE)


def paginacao(conteudo, maximo=1):
    count = 1
    lista_processo = set()
    while count <= maximo:
        proxima_pagina = conteudo.url + '?p=%d' % count
        acesso = acessar_url(proxima_pagina)
        dados = extrair_conteudo(acesso.content)
        if dados:
            for dado in dados:
                lista_processo.add(dado)
            count += 1
    return lista_processo


def _qtd_pagina(conteudo):
    if conteudo.status_code == 200:
        root = html.fromstring(conteudo.content)
        try:
            maximo = max(map(int, root.xpath('//a[@class="number"]/text()')))
            return maximo
        except:
            return 1
    else:
        print "Erro: %s" % conteudo.status_code
        return False


def principal(data_inicial, data_final, url_base, tribunal):
    dados = []
    dias = dias_da_semana(data_inicial, data_final, url_base, tribunal)
    for dia in dias:
        conteudo = acessar_url(dia)
        quantidade_paginas = _qtd_pagina(conteudo)
        for dado in paginacao(conteudo, quantidade_paginas):
            dados.append(dado)
        print dados


if __name__ == '__main__':
     datainicial = datetime(2014, 8, 21)
     datafinal = datetime(2014, 11, 10) # datetime.now()
     tribunal = "2"
     dias = dias_da_semana(datainicial, datafinal, url_base, tribunal)
     print dias[0]
     _qtd_pagina(acessar_url(dias[0]))

     """
     inicio = time.time()
     dados = principal(datainicial, datafinal, url_base, tribunal)
     final = time.time() - inicio
     for dado in dados:
         print dado
     print "Terminou em: %f seg." % final
     """