#!/usr/bin/env python
# encoding: utf-8

__author__ = 'mazza'

"""
Segundo Crawler Mazza - 06/11/2014
OAB-RJ - Caça Advogados
"""

import re
import requests
from lxml import html


url_pesquisa = 'http://www.oab-rj.org.br/resultado_busca_inscritos.php?tipo=insc&nome=2'



def coleta_dados(url):

    try:
        site = requests.get(url_pesquisa)
    except Exception as e:
        site = None
        print "Erro: ", e

    if site.status_code == 200:
        conteudo = site.content
    else:
        print "Erro: %s" % site.status_code
        conteudo = False

    if conteudo:
        miolo = html.fromstring(conteudo)

        nome_adv = miolo.xpath('//div[@class="item_pesq"]/table/tr[1]/td[2]/text()')
        num_oab = miolo.xpath('//div[@class="item_pesq"]/table/tr[2]/td[2]/text()')
        tipo_inscricao = miolo.xpath('//div[@class="item_pesq"]/table/tr[3]/td[2]/text()')
        situacao = miolo.xpath('//div[@class="item_pesq"]/table/tr[4]/td[2]/text()')


        dados_adv = zip(nome_adv, num_oab, tipo_inscricao, situacao)

        informacoes = []

        for nome, oab, inscricao, situ in dados_adv:
            if situ != 'FALECIDO' or situ != "CANCELADO":
                informacoes.append((nome, oab))
            if oab == num_oab:
                print 'duplicado'
        return informacoes



    else:
        print u"Sem conteúdo"
        return False
rever = range(1, 190000, 200)
# rever.reverse()
for x in rever:
    url_pesquisa = 'http://www.oab-rj.org.br/resultado_busca_inscritos.php?tipo=insc&nome=%d' % x
    for adv in coleta_dados(url_pesquisa):
        print adv

