# encoding: utf-8
__author__ = 'joaotrevizoliesteves'

from lxml import html

with open("TRT3_teste1.html") as file:
    data = file.read().decode('iso-8859-1').encode('utf8')

root = html.fromstring(data)
numero = unicode(root.xpath(u'//td/preceding-sibling::td[contains(text(), "Processo:")]/following-sibling::td/span/text()')[0])
classe = unicode(root.xpath(u'//td/preceding-sibling::td[contains(text(), "Natureza:")]/following-sibling::td/text()')[0]).split(u'-')[1].strip()
print(classe)