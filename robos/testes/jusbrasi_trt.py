#!/usr/bin/env python
# encoding: utf-8

# http://www.jusbrasil.com.br/diarios/TRT-2/2014/11/13/JUDICIARIO?view_page=4
from requests.packages.urllib3.exceptions import HTTPError

__author__ = 'joaotrevizoliesteves'

import re
import requests
import time
from datetime import timedelta, datetime
from lxml import html

url_base = "http://www.jusbrasil.com.br/diarios/TRT-{0}/{1}/{2}/{3}/JUDICIARIO?view_page={4}"

num_paginacao = re.compile(r'\s\/\s\d+\s')

numero_processo = re.compile(r'.*(\d).*\s(\w+)\-(\d{7}\-\d+\.\d{4}\.\d\.\d{2}\.\d{4})')


def dias_da_semana(data_inicio, data_final, url, tribunal, proibido=set([5, 6]), num_pag=1):
    dias = []
    dia = timedelta(days=1)
    if data_inicio and data_final:
        if data_final > data_inicio:
            while data_inicio <= data_final:
                if data_inicio.weekday() not in proibido:
                    dias.append(url.format(tribunal, "%02d" % data_inicio.year,
                                           "%02d" % data_inicio.month,
                                           "%02d" % data_inicio.day,
                                           "%d" % num_pag)
                    )
                data_inicio = data_inicio + dia
            return dias
        else:
            print "Data Final menor do que a inicial"
            return False
    else:
        return


def acessar_url(url):
    # Cabecalho
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0'
    }
    sessao = requests.Session()
    try:
        acesso = sessao.get(url, headers=headers)
        if acesso.status_code == 200:
            return acesso
    except HTTPError as e:
        print e
        return False


def extrair_conteudo(conteudo):
    print(numero_processo.findall(conteudo))
    return numero_processo.findall(conteudo)



def _qtd_pagina(conteudo):
    root = html.fromstring(conteudo)
    try:
        numero = root.xpath('.//*[@id="wrap"]/section/section/section/header/div[2]/div/span[6]/form/span')[0]
        numero = num_paginacao.findall(numero)
    except:
        numero = False
        pass
    return numero


def principal(data_inicial, data_final, url_base, tribunal):
    dados = []
    dias = dias_da_semana(data_inicial, data_final, url_base, tribunal)
    for dia in dias:
        for dado in paginacao(acessar_url(dia)):
            dados.append(dado)
        return dados


if __name__ == '__main__':
    datainicial = datetime(2014, 9, 16)
    datafinal = datetime(2014, 11, 10)  # datetime.now()
    tribunal = "3"
    inicio = time.time()
    dados = principal(datainicial, datafinal, url_base, tribunal)
    final = time.time() - inicio
    for dado in dados:
        print dado
    print "Terminou em: %f seg." % final