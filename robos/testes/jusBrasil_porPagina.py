#!/usr/bin/env python
# encoding: utf-8
__author__ = 'joaotrevizoliesteves'

trts = [u'TRT-1', u'TRT-2', u'TRT-3', u'TRT-4', u'TRT-6', u'TRT-7', u'TRT-8', u'TRT-9', u'TRT-10',
        u'TRT11', u'TRT12', u'TRT-13', u'TRT-14', u'TRT-15', u'TRT-16', u'TRT-17', u'TRT-18',
        u'TRT19', u'TRT20', u'TRT21', u'TRT22', u'TRT23', u'TRT24']


headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0'
}

url = "http://www.jusbrasil.com.br/diarios/{}/"