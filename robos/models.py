# encoding: utf-8


__author__ = 'murilobsd'


from mongoengine import *
from mongoengine import Q
import os
import datetime
from decimal import Decimal

if os.getenv('PROD'):
    connect('processos', host='10.132.188.84')
elif os.getenv('INTERNO'):
    connect('processos', host='db.lab804.com.br')
else:
    connect('processos')


class Movimentacoes(EmbeddedDocument):
    data = DateTimeField()
    descricao = StringField()
    arquivo = StringField()



class Partes(EmbeddedDocument):
    nome = StringField()        # Nome da parte
    tipo = StringField()        # Reu, Autor, Requerente, Requerido...
    advogados = ListField()   # Advogados
    documento = StringField(default=None)   #CPF ou CNPJ


class Processos(DynamicDocument):
    classe = StringField(required=True, default='rtord')
    numero = StringField(default=None, max_length=50, unique_with='classe')         # Numero do Processo CNJ.
    ano = StringField(default=None)
    instancia = IntField(default=1)                                                 # Instancia que pertecence o processo 1 ou 2
    tribunal = StringField(default='DJSP')                                          # Sigla TRT1, 2, 3, 4
    url = StringField(default=None)                                                 # Se o acesso for via URL
    protocolo = StringField(default=None)
    area = StringField(default='Trabalhista')                                       # Trabalhista/Civil
    assunto = StringField(default=None)
    orgao_origem = StringField(default=None)
    valor = DecimalField(default=None)
    partes =ListField(EmbeddedDocumentField(Partes))
    local_fisico = StringField(default=None)
    movimentacoes = ListField(EmbeddedDocumentField(Movimentacoes))
    criado_em = DateTimeField(default=datetime.datetime.now, required=True)
    atualizado_em = DateTimeField(default=datetime.datetime.now, required=True)
    juiz = StringField(default=None)
    distribuicao = DateTimeField(default=None)
    regiao = StringField(default=None)
    tipo_processo = StringField(default='PJE')
    ultima_movimentacao = StringField(default=None)                                 # Hash com a data e o tempo da ultima atualizacao.
    comarca = StringField(default=None)

    def __unicode__(self):
        return self.numero


    @classmethod
    def coleta_tribunal(self, tribunal, limit=None):
        if limit:
            return Processos.objects(tribunal=tribunal.upper()).limit(int(limit))
        return Processos.objects(tribunal=tribunal.upper())

    def atualizar_codigo(numero, tribunal):
        try:
            colecao = Processos.objects(Q(numero=re.compile('\d+\-\d+\.\d+\.5\.{0}\.\d+'.format(numero))))
        except:
            colecao = False
            pass
        if colecao:
            for processo in colecao:
                print "Atualizando processo de numero: %s" % processo.numero
                processo.update(set__tribunal=tribunal)

    meta = {
        'indexes': ['numero', 'atualizado_em'],
        'ordering': ['-atualizado_em']
    }


class Empresas(DynamicDocument):
    cnpj = StringField(unique=True)
    nome_fantasia = StringField()
    razao = StringField(required=True)
    data_abertura = DateTimeField()
    status = StringField()
    natureza_juridica = StringField()
    cod_natureza_juridica = StringField(max_length=5)
    rua = StringField()
    numero_rua = StringField()
    bairro = StringField()
    cidade = StringField()
    estado = StringField(max_length=2)
    cep = StringField()
    telefone = StringField()
    atividade_primaria = StringField()
    atividade_secundaria = ListField()
    criado_em = DateTimeField(default=datetime.datetime.now, required=True)
    atualizado_em = DateTimeField(default=datetime.datetime.now, required=True)
    processos = ListField(ReferenceField(Processos))

class Empresas_cadastradas(DynamicDocument):
    url = StringField(unique=True   )



class Advogado(DynamicDocument):
    inscricao = StringField(max_length=2)
    oab = StringField(unique_with=['inscricao'], required=True, max_length=30)
    nome = StringField(unique_with=['inscricao', 'oab'], unique=True, required=True, max_length=250)
    criado_em = DateTimeField(default=datetime.datetime.now, required=True)
    atualizado_em = DateTimeField(default=datetime.datetime.now, required=True)
    processos = ListField(ReferenceField(Processos))

    def __unicode__(self):
        return "%s - %s" % (self.nome, self.oab)

    def save(self, *args, **kwargs):
        if not self.atualizado_em:
            self.modified_date = datetime.datetime.now()
        return super(Advogado, self).save(*args, **kwargs)

    def primeiro_nome(self):
        return self.nome.split()[0]

    def oab_completa(self, posicao=0):
        if posicao == 0:
            return self.inscricao+self.oab
        else:
            return self.oab+self.inscricao

    meta = {
        'indexes': ['nome'],
        'ordering': ['nome']
    }

class Comarca(EmbeddedDocument):
    nome = StringField()
    numero = StringField()

class Tribunais(DynamicDocument):
    nome = StringField(unique=True, required=True, max_length=250)
    url = StringField()
    sigla = StringField()
    captcha = BooleanField()
    comarca = EmbeddedDocumentField(Comarca)
    criado_em = DateTimeField(default=datetime.datetime.now, required=True)
    atualizado_em = DateTimeField(default=datetime.datetime.now, required=True)


    def __unicode__(self):
        return self.sigla

    meta = {
        'indexes': ['nome', 'sigla'],
        'ordering': ['sigla']
    }


    def __unicode__(self):
        return "%s - %s" % (self.tipo, self.nome)

"""
# Remover os R$
var cursor = db.processos.find();
while (cursor.hasNext()) {
  var x = cursor.next();
  x['valor'] = x['valor'].replace(',', '.');
  db.processos.update({_id : x._id}, x);
}

# Remover os espacos em branco
db.processos.find({},{ "valor": 1 }).forEach(function(doc) {
   doc.valor = doc.valor.trim();
   db.processos.update(
       { "_id": doc._id },
       { "$set": { "valor": doc.valor } }
   );
})

db.processos.find({
    "$and": [
        { "valor": /^\s+/ },
        { "valor": /\s+$/ }
    ]
})

db.processos.find({ "valor": /^\s+|\s+$/ })

var batch = [];
db.processos.find({ "valor": /^\s+|\s+$/ },{ "valor": 1 }).forEach(
    function(doc) {
        batch.push({
            "q": { "_id": doc._id },
            "u": { "$set": { "valor": doc.valor.trim() } }
        });

        if ( batch.length % 1000 == 0 ) {
            db.runCommand("update", batch);
            batch = [];
        }
    }
);

if ( batch.length > 0 )
    db.runCommand("update", batch);


# Busca
curl -XGET 'http://104.236.38.24:9200/processosindex/_search?q=partes:casas bahia'

curl -XGET 'http://104.236.38.24:9200/processosindex/_search' -d '{
    "from" : 0, "size" : 2,
    "sort" : [{ "distribuicao" : {"order" : "asc"}}],
    "query" : {
        "match" : { "partes" : "casas+bahia" }
    }
}'

curl -XGET 'http://104.236.38.24:9200/processosindex/_search' -d '{
"query": { "bool": { "must": [ { "query_string": { "default_field": "processos.partes","query": "casas bahia"}},
{ "range": { "processos.valor": { "gt": "27250.00","lt": "37250.00"}}}],"must_not": [ ],"should": [ ]}},
"from": 0,
"size": 10,
"sort": [ ],
"facets": { }
}'

curl -XGET 'http://104.236.38.24:9200/processosindex/_search' -d '{
    "filtered": {
        "query": {
            "match_all": {}
        },
        "filter": {
            "regexp":{
                "processos.partes" : "\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}"
            }
        }
    }
}'

db.processos.find({}).forEach(function(doc) {
    if(doc.valor){
        print('O que que tem: ' + doc.valor);
        if(isNaN(doc.valor)) {

            db.processos.update(
               { _id: doc._id},
               { $set : { "valor" : parseFloat(doc.valor) } }
            )
        }
    }
})

"""

