from celery import task, group
from testes.jusbrasil_celery import url_base, dias_da_semana, _qtd_pagina, paginacao, acessar_url
from models import Processos
from pje import primeiro_acesso, atualizar
# from classes.classe_tjsp import RoboTJSP

"""
@task(ignore_result=True)
def com_calma(url):
    adv_semproxy = RoboTJSP(proxy_tor=True, async=False)
    dados = adv_semproxy.coleta_dados(url)
    print dados

@task(ignore_result=True, serializer='pickle', compression='zlib')
def busca_processos(numero_oab):
    print('Robo buscando: {0}'.format(numero_oab))
    adv_semproxy = RoboTJSP(proxy_tor=True, async=False)
    adv_semproxy.buscar(numero_oab)
    if adv_semproxy.advogados:
        subtasks = group(busca_processos.s(url) for url in adv_semproxy.advogados if url != numero_oab)
        subtasks()
        #if adv_semproxy.urls_403:
        #    for url in adv_semproxy.urls_403:
        #        com_calma.delay(url)
    #if adv_semproxy._quantidade > 0:
    #    return adv_semproxy.resultado
"""

@task
def get_dados(dia):
    conteudo = acessar_url(dia)
    quantidade_paginas = _qtd_pagina(conteudo)
    dados = []
    for dado in paginacao(conteudo, quantidade_paginas):
        try:
            Processos.objects.create(numero=dado.strip())
        except:
            pass
        #dados.append(dado)

@task
def principal(data_inicial, data_final, tribunal, url_base=url_base):
    dias = dias_da_semana(data_inicial, data_final, url_base, tribunal)
    job = group([get_dados.s(dia) for dia in dias])
    result = job.apply_async()
    return result.get()


@task
def trt2(numero):
    primeiro_acesso(numero)