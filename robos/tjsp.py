#!/usr/bin/env python
# encoding: utf-8

__author__ = 'mazza'

"""
Primeiro Crawler Mazza - 06/11/2014
TJSP - http://esaj.tjsp.jus.br/
"""

import re
import argparse
import sys
import time

import requests
from lxml import html

from models import Processos, Movimentacoes, Advogado


try:
    import requesocks
except:
    print "Precista instalar o modulo requesocks: sudo pip install requesocks"
    sys.exit()

url_busca_oab = 'http://esaj.tjsp.jus.br/cpo/pg/search.do;jsessionid=CD111D4A3343A94F6D4E129A4FDB26A0.cpo4?' \
                'conversationId=&paginaConsulta=1&localPesquisa.cdLocal=-1&cbPesquisa=NUMOAB&tipoNuProcesso=UNIFICADO&' \
                'dePesquisa={numOAB}'

# Expressao Regular para Casar: Nome (OAB)
cavucar_adv = re.compile(r'(((?:\s\w+)+)\s\(OAB\s(\d+\/\w{2})\))')

numero_oab = re.compile(r'\d+[A-Z]{2}')

erros_conexao = 0

use_tor = False

proxy = {'http': 'socks5://127.0.0.1:9050'}

adv = set()


def ajustar_oab(numero):
    dividir = numero.split('/')
    if len(dividir) > 1:
        # Possui uma Barra
        return numero.replace('/', '').strip()
    else:
        return False

# PROCURA OAB'S DENTRO DO PROCESSO E ADICIONA PARA PESQUISAS POSTERIORES
def cade_adv(texto, q):
    global adv
    advogados = cavucar_adv.findall(texto)
    if advogados:
        for advogado_completo, nome, oab in advogados:
            try:
                sufixo = oab[-2:]
                prefixo = oab[:-2]
                Advogado.objects.create(nome=nome, inscricao=sufixo, oab=prefixo)
                q.put(ajustar_oab(oab))
                print "Adicionando Novo Advogado para pesquisa. %s" % nome
            except:
                pass
    else:
        return False

# LIMPA A PARADA TODA
def limpeza(palavra):
    if palavra:
        return re.sub(r'\s+', ' ', palavra).strip()
    else:
        print u"Não temos a palavra"
        return False

# AQUI É ONDE O BANGUE ACONTECE. CONECTA COM AS MUTRETAS DO TOR + LÊ O HTML + XPATH NOS CAMPOS + RESGATA OS VALORES
def coleta_dados(url, tribunal, r, q, num=None):
    '''
    url = str()
    '''
    resultado = {}
    global proxy
    global erros_conexao
    global use_tor
    try:
        if erros_conexao < 5 and not use_tor:
            site = requests.get(url, timeout=5)
        else:
            sessao = requesocks.session()
            sessao.proxies = proxy
            site = sessao.get(url, timeout=5)
    except Exception as e:
        r.incr("timeouts")
        site = None
        erros_conexao += 1
        print "Erro: ", e

    if site.status_code == 200:
        conteudo = site.content
    elif site.status_code == 403:
        return
    else:
        print "Erro!: %s" % site.status_code
        conteudo = False

    if conteudo:
        partes_conteudo = ""
        miolo = html.fromstring(conteudo)
        processo = miolo.xpath('//td/span[@class=""][1]/text()')[0]
        area = miolo.xpath('//td/span[@class="labelClass"]/../text()')[1].strip()
        classe_processo = miolo.xpath('//td/span/span/text()')[0]
        try:
            assunto = miolo.xpath('//tr/td/div[contains(text(), "Assunto")]/../../td/span/text()')[0]
        except:
            assunto = ""
        distribuicao = miolo.xpath('//tr/td/div[contains(text(), "Distribui")]/../../td/span/text()')[0]
        try:
            origem = \
                miolo.xpath('//tr/td/div[contains(text(), "Distribui")]/ancestor::tr/following::tr/td/span/text()')[0]
        except:
            origem = False
        try:
            local = miolo.xpath('//tr/td/div[contains(text(), "Local")]/../../td/span/text()')[0]
        except:
            local = ""
        try:
            juiz = miolo.xpath('//tr/td/div[contains(text(), "Juiz")]/../../td/span/text()')[0]
        except:
            juiz = ""
        try:
            valor = miolo.xpath('//tr/td/div[contains(text(), "Valor")]/../../td/span/text()')[0]
        except:
            valor = ""
        linhas = miolo.xpath('//table[contains(@id, "tabelaTodasMovimentacoes")]/tr')

        # Se a tabelaTodasPartes existe otimo.
        if miolo.xpath('.//table[@id="tableTodasPartes"]/tr'):
            linhas2 = miolo.xpath('.//table[@id="tableTodasPartes"]/tr')
            partes = []
            for linha in linhas2:
                partes.append((limpeza(linha.xpath('./td[1]/span[@class="mensagemExibindo"]/text()')[0]),
                               limpeza(" ".join(linha.xpath('./td[2]//text()')))))
        else:
            linhas2 = miolo.xpath('.//table[@id="tablePartesPrincipais"]/tr')
            partes = []
            for linha in linhas2:
                partes.append((limpeza(linha.xpath('./td[1]/span/text()')[0]),
                               limpeza(" ".join(linha.xpath('./td[2]//text()')))))

        movimentacoes = []
        for linha in linhas:
            data = limpeza(linha.xpath('td[1]/text()')[0])
            descricao = limpeza(" ".join(linha.xpath('td[3]//text()')))
            movimentacoes.append((data, descricao))
            cade_adv(descricao, q)  # Procurando por advogados e adicionadno na variavel global adv

        resultado['tribunal'] = tribunal
        resultado['url'] = url
        resultado['numero'] = limpeza(processo)
        resultado['area'] = area
        resultado['classe'] = classe_processo
        resultado['assunto'] = assunto
        resultado['distribuicao'] = distribuicao
        resultado['local_fisico'] = local
        resultado['juiz'] = juiz
        resultado['valor'] = valor
        resultado['orgao_origem'] = origem
        for parte in partes:
            partes_conteudo += " " + " ".join(parte)
        resultado['partes'] = partes_conteudo
        resultado['movimentacoes'] = [Movimentacoes(data=data, descricao=dados) for data, dados in movimentacoes]
        return resultado
    else:
        print u"Sem conteúdo"
        return False

# LISTA OS LINKS DOS PROCESSOS DA BUSCA
def lista_processos(conteudo):
    if conteudo:
        miolo = html.fromstring(conteudo)
        links = miolo.xpath('//a[@class="linkProcesso"]/@href')
        return links
    else:
        print u"Não tem conteúdo"
        return False

# VERIFICA SE EXISTE PAGINAÇÃO NOS RESULTADOS. E VAI PASSANDO POR TODAS AS PÁGINAS EXISTENTES
def paginacao(numOAB, r, q):
    global erros_conexao
    global proxy
    global use_tor
    resultado = []
    num_paginacao = 0
    url_base = 'http://esaj.tjsp.jus.br'
    try:
        if erros_conexao < 5 and not use_tor:
            site = requests.get(url_busca_oab.format(numOAB=numOAB), timeout=5)
        else:
            print "Usando Tor!"
            sessao = requesocks.session()
            sessao.proxies = proxy
            site = sessao.get(url_busca_oab.format(numOAB=numOAB), timeout=5)
    except Exception as e:
        r.incr("timeouts")
        site = None
        erros_conexao += 1
        print "Erro: ", e

    if site.status_code == 200:
        conteudo = site.content
        miolo = html.fromstring(conteudo)
        links = lista_processos(conteudo)
        if links:
            for link in links:
                num_paginacao += 1
                # print u"Paginação: %d" % num_paginacao
                resultado.append(coleta_dados(url_base + link, 'TJSP', r, q))
                time.sleep(1)
        else:
            print u"Não existe mais links."
            return False
        fimPaginacao = miolo.xpath('//a[contains(text(), ">")]')
        if fimPaginacao:
            while fimPaginacao:
                proxPagina = miolo.xpath('//a[contains(text(), ">")]/@href')[0]
                try:
                    site = requests.get(url_base + '/cpo/pg/' + proxPagina)
                except Exception as e:
                    site = None
                    r.incr("timeouts")
                    print "Erro: ", e
                conteudo = site.content
                miolo = html.fromstring(conteudo)
                links = lista_processos(conteudo)
                if links:
                    for link in links:
                        num_paginacao += 1
                        resultado.append(coleta_dados(url_base + link, 'TJSP', r, q))
                        time.sleep(1)
                        # print u"Paginação: %d" % num_paginacao
                else:
                    print u"Não possui mais links"
                    return False
                fimPaginacao = miolo.xpath('//a[contains(text(), ">")]')
        return resultado
    else:
        print "Erro!"


def inserir_db(resultado, r):
    try:
        Processos.objects.create(tribunal=resultado['tribunal'],
                                 url=resultado['url'],
                                 numero=resultado['numero'],
                                 area=resultado['area'],
                                 assunto=resultado['assunto'],
                                 distribuicao=resultado['distribuicao'],
                                 local_fisico=resultado['local_fisico'],
                                 juiz=resultado['juiz'],
                                 valor=resultado['valor'],
                                 partes=resultado['partes'],
                                 movimentacoes=resultado['movimentacoes'])
        r.incr('sucesso')
    except:
        r.incr('duplicados')
        pass


def main(numero):
    try:
        numero_oab.match(numero).group(0)
        resultado = paginacao(numero)
        if resultado:
            # print list(adv)
            for res in resultado:
                inserir_db(res)

    except Exception as e:
        print u"Número OAB Inválido.", e


def principal(q, r):
    for item in q.get():
        if not item:
            print "RECEIVED SENTINEL"
            # received sentinel
            break
        print "Buscando Processos: %s" % item
        try:
            numero_oab.match(item).group(0)
            resultado = paginacao(item, r, q)
            if resultado:
                for res in resultado:
                    inserir_db(res, r)
        except Exception as e:
            r.incr("erros")
            print u"Número OAB Inválido.", e


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=u'*** Crawler Mazza lab804 TJSP ***', version='%(prog)s 1.0')
    parser.add_argument('-n', dest='numero', required=True, type=str, help=u'Coloque o Número da OAB exemplo: 88548SP.')
    parser.add_argument('-t', dest='tor', type=bool, help=u'Essa opção é para iniciar usando o tor.')
    args = parser.parse_args()
    if args.tor:
        use_tor = True
    main(args.numero.strip())
