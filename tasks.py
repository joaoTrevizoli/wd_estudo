# encoding: utf-8

import os
from celery import Celery, group
from celery.utils.log import get_task_logger
from celery.schedules import crontab
from robos.models import Processos
import re
import datetime

from robos.classes.pje import primeiro_acesso
from robos import jusbrasil

__author__ = 'murilobsd'

if os.getenv('PROD'):
    app = Celery('tasks', backend='amqp://zombie:D1g1n3tT@@mestre//',
                 broker='amqp://zombie:D1g1n3tT@@mestre//')
elif os.getenv('INTERNO'):
    app = Celery('tasks', backend='amqp://zombie:D1g1n3tT@@mestre.lab804.com.br//',
                 broker='amqp://zombie:D1g1n3tT@@mestre.lab804.com.br//')
else:
    app = Celery('tasks', backend='amqp://',
                 broker='amqp://')

app.conf.update(
    CELERY_TASK_SERIALIZER='json',
    CELERY_ACCEPT_CONTENT=['json'],  # Ignore other content
    CELERY_RESULT_SERIALIZER='json',
    CELERY_TIMEZONE='America/Sao_Paulo',
    #CELERY_ENABLE_UTC=True,
    CELERY_ANNOTATIONS={'tasks.trt': {'rate_limit': '20/s'}},
    CELERYBEAT_SCHEDULE={
        'add-every-monday-morning': {
            'task': 'tasks.datas_trt_jus',
            'schedule': crontab(hour=22, minute=53, day_of_week='wed'),
            'args': (),
        }
    },
    CELERY_IGNORE_RESULT=True
)

logger = get_task_logger(__name__)


@app.task(bind=True)
def error_handler(self, uuid):
    result = self.app.AsyncResult(uuid)
    print('Tarefa {0} erro: {1!r}\n{2!r}'.format(
        uuid, result.result, result.traceback))


@app.task
def trt(numero_processo, tribubal):
    dados = primeiro_acesso(numero_processo, tribubal)
    return dados


@app.task
def executar(dados):
    num = re.compile(dados)
    data = datetime.datetime.now
    processos = Processos.objects(numero=num, atualizado_em__lt=data).only('numero', 'tribunal')
    if processos:
        for processo in processos:
            trt.apply_async((processo.numero, processo.tribunal))


@app.task(ignore_result=True)
def paginacao_trt_jus(url, tribunal):
    jusbrasil.paginacao(url, tribunal)


@app.task(ignore_result=True)
def datas_trt_jus():
    dados = jusbrasil.data()
    if dados:
        for dado in dados:
            url = dado['url']
            tribunal = dado['tribunal']
            jusbrasil.paginacao(jusbrasil.acessar_url(url), tribunal)