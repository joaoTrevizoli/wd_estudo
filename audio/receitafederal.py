#!/usr/bin/env python
# encoding: utf-8

__author__ = 'murilobsd'

import requests
from requests.packages.urllib3.exceptions import HTTPError
from lxml import html
import argparse
import time
import re


url_base = 'http://www.receita.fazenda.gov.br/aplicacoes/atcta/cpf/ConsultaPublica.asp'

url_audio = 'http://captcha2.servicoscorporativos.serpro.gov.br/captcha/1.0.0/som/' \
            'wav?id={1}&token={0}'

url_captcha = 'http://captcha2.servicoscorporativos.serpro.gov.br/captcha/1.0.0/imagem'

def acessar_url(url, method='get', parametros=(), stream=False):
    headers = {
        'Origin': 'http://www.receita.fazenda.gov.br',
        'Pragma': 'no-cache',
        'Referer':'http://www.receita.fazenda.gov.br/aplicacoes/atcta/cpf/ConsultaPublica.asp',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0'
    }

    sessao = requests.Session()
    try:
        if method == 'get':
            if stream:
                acesso = sessao.get(url, headers=headers, verify=False, stream=True)
            else:
                acesso = sessao.get(url, headers=headers, verify=False)

        if method == 'post':
            if stream:
                acesso = sessao.post(url, headers=headers, verify=False, stream=True, data=parametros)
            else:
                acesso = sessao.post(url, headers=headers, verify=False, data=parametros)

        if acesso and acesso.status_code == 200:
            return acesso
    except HTTPError as e:
        print e
        return False

def coletar_id(conteudo):
    root = html.fromstring(conteudo.content)
    try:
        codigo2 = root.xpath('//div[@id="captchaConsutlaSituacao"]/div/@data-clienteid')[0]
        acesso = acessar_url(url_captcha, method='post', parametros=codigo2)
        codigo1 = re.findall(r'^(\d+)\@', acesso.content)[0]
    except Exception as e:
        print "Erro: ", e
        codigo2 = False
        codigo1 = False
    return codigo1, codigo2


def salvar_audio(codigo, codigo2, nome):
    if codigo and codigo2:
        arquivo = acessar_url(url_audio.format(codigo, codigo2), stream=True)
        with open(nome, 'wb') as f:
            for chunk in arquivo.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
                    f.flush()
        return nome
    else:
        return False


def principal(inicio=0, fim=100, intervalo=5):
    count = inicio
    while count < fim:
        site = acessar_url(url_base)
        codigo1, codigo2 = coletar_id(site)
        print codigo1, codigo2
        salvar_audio(codigo1, codigo2, 'teste_%d.wav' % count)
        print "Salvando arquivo %d" % count
        count += 1
        time.sleep(10)


principal(inicio=7, fim=100)