/**
 * Created by murilobsd on 19/11/14.
 */
var app = angular.module("app", ['ngAnimate']);

app.filter('date', function($filter)
{
    return function(input)
    {
        if(input == null){ return ""; }
        var _date = $filter('date')(new Date(input), 'dd/MM/yyyy HH:mm:ss');
        return _date;
    };
});

app.filter('dataFilter', function() {
    return function(people, searchText) {
      var regexp = new RegExp(searchText, 'i');
      return people.filter(function(person) {
        var found = false;
        Object.keys(person).some(function(key) {
              console.log(searchText);
            found = person[key].search(regexp) > -1;
            return found;
        });
        return found;
      });
    };
});

app.filter('capitalize', function() {
    return function(input, all) {
      return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
  });

app.filter("timeAgo", function() {
  return function(date) {
    return jQuery.timeago(date);
  };
});

app.controller("AppCtrl", function($scope, $http) {
    $scope.processo = null;
    $scope.tableSearch = '';

    $http.get("http://wd.lab804.com.br/api/processos/0021100-70.2013.5.04.0405")
      .success(function(data) {
                $scope.processo = data;
            });
})