# encoding: utf-8
from flask import Flask, request, render_template
import json
import requests
from models import Processos, Advogado

__author__ = 'murilobsd'

app = Flask(__name__)

"""
@app.route("/referencias", methods=['GET'])
def busca():
    query1 = {"query":
                  {"bool":
                       {"must":[{"query_string":
                                     {"default_field":"_all","query":""}}],
                        "must_not":[],"should":[]}},"from":0,"size":10,"sort":[],"facets":{}}
    query2 = {
        "query" : {
            "filtered" : {
                "query" : {
                    "match_all" : {}
                },
                "filter" : {
                    "term" : {
                        "processos.partes.advogados" : "neide"
                    }
                }
            }
        }
    }
    advogado = request.args.get('q', None)

    if not advogado:
        return render_template('listagem.html', data=[], adv="", total=0)

    query1['query']['bool']['must'][0]['query_string']['query'] = advogado
    query2['query']['filtered']['filter']['term']['processos.partes.advogados'] = advogado
    print query2
    es_query = json.dumps(query2)
    uri = 'http://104.236.38.24:9200/processosindex/processos/_search'
    r = requests.get(uri, data=es_query)
    results = json.loads( r.text )
    data = results['hits']['hits']
    n = []
    print data
    for d in data:
        b = {}
        b['numero'] = d['_source']['numero']
        try:
            b['classe'] = d['_source']['classe']
        except:
            b['classe'] = u'Não Possui'
        try:
            b['instancia'] = d['_source']['instancia']
        except:
            b['instancia'] = u'Não Possui'
        try:
            b['tribunal'] = d['_source']['tribunal']
        except:
            b['tribunal'] = u'Não Possui'
        try:
            b['movimentacao'] = d['_source']['movimentacoes'][0]['data']
        except:
            b['movimentacao'] = u'Não Possui'
        n.append(b)
    total = results['hits']['total']
    return render_template('listagem.html', data=n, adv=advogado, total=total)
"""

@app.route("/referencias", methods=['GET'])
def busca():
    advogado = request.args.get('q', None)
    if not advogado:
        return render_template('listagem.html', adv=advogado, total=0)
    try:
        adv = Advogado.objects.get(nome=advogado.lower())
    except:
        adv = None
    if not adv:
        return render_template('listagem.html', adv=advogado, total=0)
    return render_template('listagem.html',adv=adv, total=len(adv.processos))


@app.route("/referencias/processo/<numero>", methods=['GET'])
def processo(numero):
    processo = Processos.objects.get(numero=numero)
    return render_template('resultado_busca.html', processo=processo)

if __name__ == '__main__':
    app.run(debug=True)

# uwsgi_param UWSGI_SETENV PROD=yes;