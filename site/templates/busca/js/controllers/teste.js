searchApp.controller('Teste', function ($scope, ejsResource) {
    var ejs = ejsResource('http://wd.lab804.com.br/pesquisar');
    var index = 'processosindex';

    var highlightPost = ejs.Highlight(["text"])
        .fragmentSize(150, "text")
        .numberOfFragments(1, "text")
        .preTags("<b>", "text")
        .postTags("</b>", "text");

    var hashtagFacet = ejs.TermsFacet('Hashtag')
        .field('hashtag.text')
        .size(10);

    var statusRequest = ejs.Request()
        .indices(index)
        .types('processos')
        .highlight(highlightPost)
        .facet(hashtagFacet);

    var activeFilters = {};

    $scope.resultsArr = [];

    $scope.search = function () {
        activeFilters = {};
        $scope.resultsArr = [];
        if (!$scope.queryTerm == '') {
            results = statusRequest
                .query(applyFilters(ejs.MatchQuery('_all', $scope.queryTerm)))
                .fields(['_id', 'numero', 'assunto', 'atualizado_em'])
                .doSearch();

            $scope.resultsArr.push(results);
        } else {
            results = {};
            $scope.resultsArr = [];
            activeFilters = {};
        }
    };


    $scope.renderResult = function (result) {
        // console.log(result);
        var resultText = "";
        if (result.highlight)
            resultText = result.highlight.text[0];
        else if (result.fields.text)
            resultText = result.fields.text;
        else
            resultText = result._id;
        return resultText;
    };

     var applyFilters = function(query) {

        var filter = null;
        var filters = Object.keys(activeFilters).map(function(k) { return activeFilters[k]; });
        // console.log(activeFilters)
        // if more than one filter, use AND operator
        if (filters.length > 1) {
            filter = ejs.AndFilter(filters);
        } else if (filters.length === 1) {
            filter = filters[0];
        }

        return filter ? ejs.FilteredQuery(query, filter) : query;
    };

});