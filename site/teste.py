from flask import Blueprint, request
admin = Blueprint('buscar', __name__)

@admin.route('/')
def index():
    advogado = request.arguments.get('q', None)
    return advogado

"""
import requests
import json


query1 = {"query":
              {"bool":
                   {"must": [
                       {"query_string":
                                  {"default_field":
                                       "processos.partes.advogados", "query": "rafael da silva ijanc"
                                  }
                       }],
                    "must_not": [], "should": []}}, "from": 0, "size": 10, "sort": [], "facets": {}}

nome = query1['query']['bool']['must'][0]['query_string']['default_field']['processos.partes.advogados']['query'] = "rafael da silva ijanc"
print nome
es_query = json.dumps(query1)
uri = 'http://104.236.38.24:9200/_search'
r = requests.get(uri, params=es_query)
results = json.loads(r.text)
data = [res['_source']['numero'] for res in results['hits']['hits']]
print data
"""