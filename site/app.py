from eve import Eve
from eve.auth import TokenAuth

class RolesAuth(TokenAuth):
    def check_auth(self, token,  allowed_roles, resource, method):
        accounts = app.data.driver.db['accounts']
        lookup = {'token': token}
        if allowed_roles:
            lookup['roles'] = {'$in': allowed_roles}
        account = accounts.find_one(lookup)
        return account

app = Eve(auth=RolesAuth)
app.config['X_DOMAINS'] = '*'

if __name__ == '__main__':
    app.run(debug=False)
