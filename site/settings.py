# encoding: utf-8
import os

XML = False
JSON = True
MONGO_HOST = 'db.lab804.com.br'
MONGO_PORT = 27017
MONGO_USERNAME = ''
MONGO_PASSWORD = ''
MONGO_DBNAME = 'processos'
JSON_SORT_KEYS = True
DATE_FORMAT = '%d/%m/%Y %H:%M:%S'
SERVER_NAME = 'wd.lab804.com.br'
URL_PREFIX = 'api'
LAST_UPDATED = 'atualizado_em'
DATE_CREATED = 'criado_em'
X_HEADERS=['Accept', 'Content-type', 'Authorization']
X_EXPOSE_HEADERS=X_HEADERS
RESOURCE_METHODS = ['GET']
ITEM_METHODS = ['GET']
PAGINATION_DEFAULT = 10
HATEOAS = False
CACHE_CONTROL = 'max-age=20'
CACHE_EXPIRES = 20

# ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL', 'http://10.132.187.119:9200')
# ELASTICSEARCH_INDEX = os.environ.get('ELASTICSEARCH_INDEX', 'processosindex')

processos = {
    'item_title': 'processo',

    'additional_lookup': {
        'url': 'regex("\d+\-\d+\.\d+\.\d+\.\d+\.\d+")',
        'field': 'numero'
    },

    'schema': {
        'numero': {
            'type': 'string',
        },
        #'instancia': {'type': 'int'},
        #'orgao_origem': {'type': 'string'},
        #'local_fisico': {'type': 'string'},
        #'protocolo': {'type': 'string'},
        'tribunal': {
            'type': 'string',
        },
        'assunto': {
            'type': 'string',
            'maxlength': 250
        },
        'regiao': {'type': 'string'},
        #'valor': {
        #    'type': 'string',
        #    'maxlength': 250
        #},
        #'classe': {
        #    'type': 'string',
        #    'maxlength': 250
        #},
        #'area': {
        #    'type': 'string',
        #    'maxlength': 250
        #},
        #'juiz': {
        #    'type': 'string',
        #    'maxlength': 250
        #},
        #'distribuicao': {
        #    'type': 'string',
        #    'maxlength': 250
        #},
        'movimentacoes': {
            'type': 'list',
            'schema': {
                'type': 'dict',
                'schema': {
                    'data': {'type': 'datetime'},
                    'descricao': {'type': 'string'}
                }
            }
        },
        #'partes': {
        #    'type': 'list',
        #    'schema': {
        #        'type': 'dict',
        #        'schema': {
        #            'nome': {'type': 'string'},
        #            'tipo': {'type': 'string'},
        #            'advogados': {'type': 'string'}
        #        }
        #    }
        #},
        'born': {
            'type': 'datetime',
        },
    }

}

advogado = {
    'item_title': 'advogado',

    'additional_lookup': {
        'url': 'regex("(?:(\w\s?)+)")',
        'field': 'nome'
    },
    'allowed_roles': ['user'],
    'schema': {
        'nome': {'type': 'string'},
        'inscricao':{'type': 'string'},
        'oab': {'type:': 'string'},
        #'data_relation': {
        #     'resource': 'processos',
        #     'field': 'numero',
        #     'embeddable': True
        # },
        'born': {
            'type': 'datetime',
        },
    },
    'datasource': {
        'projection': {'inscricao': 0, 'oab': 0, 'processos': 0},
        'default_sort': [('atualizado_em', -1)],
    }
}

accounts = {
    #'additional_lookup': {
    #    'url': '[\w]+',
    #    'field': 'username',
    #},

    'cache_control': '',
    'cache_expires': 0,

    'allowed_roles': ['superuser', 'admin'],

    'extra_response_fields': ['token'],

    'schema': {
        'username': {
            'type': 'string',
            'required': True,
            'unique': True,
        },
        'email': {
            'type': 'string',
            'required': True,
            'unique': True,
        },
        'password': {
            'type': 'string',
            'required': True,
        },
        'roles': {
            'type': 'list',
            'allowed': ['user', 'superuser', 'admin'],
            'required': True,
        },
        'token': {
            'type': 'string',
            'required': True,
        }

    }
}

DOMAIN = {
    'processos': processos,
    'advogado': advogado
}


