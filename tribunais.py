# encoding: utf-8

__author__ = 'murilobsd'

import requests
from lxml import html
import re

class Tribunal(object):

    def __init__(self, nome, url, cidade,):
        self.nome = nome
        self.url = url
        self.cidade = re.sub(r'[()]', '', cidade)


class Tribunais(object):
    url = 'http://www.cnj.jus.br/portais-dos-tribunais'

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0'
    }

    tribunal = {'trabalho': '//a[contains(text(), "Tribunal Regional do Trabalho")]/text()|'
                            '//a[contains(text(), "Tribunal Regional do Trabalho")]/@href|'
                            '//a[contains(text(), "Tribunal Regional do Trabalho")]/../span/text()'}

    def __init__(self):
        self.sessao = requests


    def get_tribunal(self, tipo):
        site = self.sessao.get(self.url).content
        root = html.fromstring(site)
        tribunais = root.xpath(self.tribunal[tipo])
        teste = [Tribunal(url=tribunais[x], nome=tribunais[x+1], cidade=tribunais[x+2])
                 for x in range(0, len(tribunais), 3)]

        return teste

tribunais = Tribunais().get_tribunal(tipo='trabalho')

print u"%d - Tribunais do Trabalho Encontrado." % len(tribunais)
for tribunal in tribunais:
    print u"%s \t%s" % (tribunal.url, tribunal.cidade)